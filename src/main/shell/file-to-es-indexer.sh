#!/bin/bash

# Filename: file-to-es-indexer.sh

# Grab all the command line arguments
while getopts ":m:" o; do
    case "${o}" in
        m)
            FTESI_BASEDIR_MOUNT=${OPTARG}
            ;;
        *)
            ;;
    esac
done
shift $((OPTIND-1))

# Check to see if we know where the directory is that will be used to write log and data files
if [ -z "$FTESI_BASEDIR_MOUNT" ]
then
  echo "You need to specify the directory where the log and data files will be written."
  echo "You can either use the -m flag or the FTESI_BASEDIR_MOUNT environment variable and the"
  echo "value should be the fully qualified path to the directory (without an ending slash)"
  echo "where the logs and data will go."
  exit 0
fi

# Since we have a log directory and repo name, log the start of the run
logger -s "Starting file-to-es-indexer script ..." 2>> $FTESI_BASEDIR_MOUNT/logs/file-to-es-indexer.log
logger -s `date +"%Y-%m-%d %T"` 2>> $FTESI_BASEDIR_MOUNT/logs/file-to-es-indexer.log
logger -s "Sleeping for 60 seconds to let other services start..." 2>> $FTESI_BASEDIR_MOUNT/logs/file-to-es-indexer.log

sleep 60

logger -s "OK, firing up the indexer" 2>> $FTESI_BASEDIR_MOUNT/logs/file-to-es-indexer.log

# Start up file-to-es-indexer java program
java -cp ./file-to-es-indexer/target:./file-to-es-indexer/target/file-indexer-1.0-SNAPSHOT-jar-with-dependencies.jar org.mbari.resources.FileCrawler