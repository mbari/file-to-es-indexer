package org.mbari.resources;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.CRC32;

import javax.xml.bind.DatatypeConverter;

import com.google.protobuf.Message;
import com.thebuzzmedia.exiftool.ExifTool;
import com.thebuzzmedia.exiftool.ExifToolBuilder;
import com.thebuzzmedia.exiftool.Tag;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.sax.BodyContentHandler;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.json.XML;
import org.mbari.model.Data;
import org.mbari.model.Resource;
import org.xml.sax.SAXException;

import ucar.nc2.Attribute;
import ucar.nc2.Dimension;
import ucar.nc2.NetcdfFile;
import ucar.nc2.Variable;

/**
 * This class can be used to index <code>File</code> objects into an
 * Elasticsearch database.
 */
public class ResourceIndexer {

    // Create the logger
    private static Logger logger = LogManager.getLogger(ResourceIndexer.class);

    // The Tika extraction client
    private Tika tika = new Tika();

    // The ElasticsearchDAO used to work with Elasticsearch
    private ElasticsearchDAO elasticsearchDAO;

    // This is a HashMap that maps indexNames to temporary local directories that
    // are being used to index files if
    // they are network mounted files
    private HashMap<String, Path> indexTempDirs = new HashMap<>();

    // Various patterns needed
    // One for scientific notation
    Pattern sciNotationPattern = Pattern
            .compile("^(-?[1-9](\\.\\d+)?)((\\s?[X*]\\s?10[E^]([+-]?\\d+))|(E([+-]?\\d+)))$");

    // One for decimals (has to be decimal)
    Pattern decimalOnlyPattern = Pattern.compile("^[+-]?\\d*(([,.]\\d{3})+)?([.]\\d+){1}$");
    Pattern decimalAtStartPattern = Pattern.compile("^[+-]?\\d*(([,.]\\d{3})+)?([.]\\d+)\\D+");

    // Patterns for integers only
    Pattern integerOnlyPattern = Pattern.compile("^[+-]?\\d*(([,]\\d{3})+)?");

    // A CRC32 calculator
    private CRC32 crc32 = new CRC32();

    /**
     * This is the constructor that takes in an <code>ElasticsearchDAO</code> object
     * that it will use to index <code>File</code>s into <code>Resource</code>s in
     * the Elasticsearch database.
     *
     * @param elasticsearchDAO
     */
    public ResourceIndexer(ElasticsearchDAO elasticsearchDAO) {

        // Set the Elasticsearch DAO
        this.elasticsearchDAO = elasticsearchDAO;
    }

    /**
     * This method takes in the path to a local file, a name to use as an
     * Elasticsearch index, an array of strings that serve as locator bases for
     * files that are indexed, and a root path (local) that is used to calculate
     * relative paths for other information. This method will check to see if the
     * file is already indexed in Elasticsearch and if so, it will check to see if
     * last modification date and/or file size have changed since it was last
     * indexed. If they have, it will re-index the file into Elasticsearch (or index
     * it for the first time if it does not exist).
     *
     * @param fileToIndex  is the <code>Path</code> to the file that is to be
     *                     indexed into Elasticsearch
     * @param repoName     is the name that is to be used as the Elasticsearch index
     *                     where the documents will be stored
     * @param locatorBases is an array of strings that are base paths for unique
     *                     locators for the files to be indexed. Think of them as
     *                     base URLs that should point to the same location as
     *                     rootPath. The files unique locators will then be
     *                     constructed using these bases and their relative paths to
     *                     the rootPath.
     * @param rootPath     is the part of the file's full path that is considered
     *                     the root and is the base of the 'repository' and points
     *                     to the same location that is referenced by the
     *                     locatorBases properties.
     * @param networkMount is a <code>boolean</code> indicating if the file is
     *                     mounted from a network share. If so, this method will
     *                     make a local copy of the file first to speed up Tika
     *                     indexing.
     */
    public void indexFile(Path fileToIndex, String repoName, String[] locatorBases, String rootPath,
            boolean networkMount) {

        logger.debug("============================================================================");

        // Make sure the incoming file can be indexed
        if (fileToIndex == null || !fileToIndex.toFile().exists()) {
            if (fileToIndex != null) {
                logger.error("The file {} does not appear to exist, so will not index", fileToIndex.toAbsolutePath());
            } else {
                logger.error("fileToIndex was null, nothing will be indexed");
            }
        } else {
            logger.debug("indexFile called with file: {}", fileToIndex.toAbsolutePath());

            // Verify a repo name was supplied
            if (repoName == null || repoName.equals("")) {
                logger.error("No repoName was supplied, this is required to index a file. Nothing will be done.");
            } else {
                logger.debug("repoName: {}", repoName);

                // Verify at least one locator base was supplied
                if (locatorBases != null && locatorBases.length > 0) {
                    logger.debug("locatorBases:");
                    for (String locatorBase : locatorBases) {
                        logger.debug(locatorBase);
                    }

                    // Log the root path supplied
                    logger.debug("rootPath: {}", rootPath);

                    // Create the resource builder that will be used to create a Resource that
                    // represents the file
                    Resource.Builder resourceBuilder = Resource.newBuilder();

                    // The first thing we want to do is populate the file 'stat' information.
                    extractFileStats(fileToIndex.toFile(), locatorBases, rootPath, resourceBuilder);

                    // Grab the locator array so we can search for an existing document
                    String[] locators = resourceBuilder.getLocatorsList()
                            .toArray(new String[resourceBuilder.getLocatorsCount()]);

                    // Now try to get the File stats from Elasticsearch to see if the file has been
                    // indexed before
                    JSONObject fileStats = elasticsearchDAO.getFileStatsFromLocators(repoName, locators);

                    logger.debug("Going to check to see if file {} needs indexing", fileToIndex.toAbsolutePath());
                    // Let's first compare the last modified and file size and if both are equal, we
                    // don't need to index
                    if (fileStats != null && fileStats.has("file") && fileStats.getJSONObject("file").has("file_size")
                            && fileStats.getJSONObject("file").has("last_modified") && resourceBuilder.getFile() != null
                            && (fileStats.getJSONObject("file").getLong("last_modified") == resourceBuilder.getFile()
                                    .getLastModified())
                            && (fileStats.getJSONObject("file").getLong("file_size") == resourceBuilder.getFile()
                                    .getFileSize())) {
                        logger.debug("Timestamps and file sizes are equal so it does not need indexing");
                    } else {
                        logger.debug("Yep, the Resource needs indexing, will do that now.");
                        // Make sure it's a file before trying to extract Tika metadata
                        if (!fileToIndex.toFile().isDirectory()) {

                            // First set the file to index as the incoming file
                            Path fileThatWillBeIndexed = fileToIndex;

                            // A local temp file if network mounted
                            File tempFile = null;

                            // Next, let's check to see if the file is on a network share
                            if (networkMount) {

                                // First try to look up the local temp directory from the HashMap
                                Path tempDir = indexTempDirs.get(repoName);

                                // If one was not found, create a temp dir and put it in the HashMap
                                if (tempDir == null) {
                                    try {
                                        tempDir = Files.createTempDirectory("resource-indexer-" + repoName + "-");
                                        indexTempDirs.put(repoName, tempDir);
                                        logger.debug("Created temporary directory for repo {} to cache network files",
                                                repoName);
                                    } catch (IOException e) {
                                        logger.error("IOException caught trying to create a temp directory for repo {}",
                                                repoName);
                                        logger.error(e.getMessage());
                                    }
                                }

                                // If a temp directory is there, try to create a temp file
                                if (tempDir != null) {
                                    // Create a temp file locally
                                    String tempFilePath = tempDir.toFile().getAbsolutePath() + File.separator
                                            + fileToIndex.toFile().getName();
                                    tempFile = new File(tempFilePath);
                                    logger.debug("Copying file {} to {} for local indexing",
                                            fileToIndex.toAbsolutePath(), tempFile.toPath());
                                    Date start = new Date();
                                    // Now copy over the network file to the local file
                                    try {
                                        Files.copy(fileToIndex, tempFile.toPath(), StandardCopyOption.COPY_ATTRIBUTES);
                                        fileThatWillBeIndexed = tempFile.toPath();
                                    } catch (IOException e) {
                                        logger.error("IOException trying to copy file {} to {} a local file: {}",
                                                fileToIndex.toAbsolutePath(), tempFile.toPath(), e.getMessage());
                                    }
                                    Date end = new Date();
                                    logger.debug("Copying file takes {} milliseconds",
                                            (end.getTime() - start.getTime()));
                                }
                            }

                            // Next, let's extract all the metadata using Tika
                            extractMetadata(fileThatWillBeIndexed.toFile(), resourceBuilder);

                            // Call the method to extract content tree if possible
                            extractContentTree(fileThatWillBeIndexed.toFile(), resourceBuilder);

                            // Go ahead and call the method to try and extract any variables from the file,
                            // it won't do anything
                            // if it doesn't think there are any variables in the file.
                            extractVariables(fileThatWillBeIndexed.toFile(), resourceBuilder);

                            // If the file was network mounted and a temp file was created, delete the local
                            // file
                            if (networkMount && tempFile != null) {
                                try {
                                    tempFile.delete();
                                } catch (Exception e) {
                                    logger.error("Exception caught trying to delete the temp file {}: {}",
                                            tempFile.toPath(), e.getMessage());
                                }
                            }
                            logger.debug("Tika and variable extraction complete");
                        } else {
                            logger.debug("Resource is a directory, so no metadata extraction occurred");
                        }

                        // Now go ahead and hand off the file to the elasticsearch DAO to do it's thing
                        String id = elasticsearchDAO.indexResource(repoName, resourceBuilder.build(), null);
                        logger.info("File {} was indexed in Elasticsearch with ID: {}", fileToIndex.toAbsolutePath(),
                                id);
                    }
                } else {
                    logger.error("No locator bases specified, no indexing will occur.");
                }
            }
        }
    }

    /**
     * This method takes in a <code>File</code> object which will be examined and
     * have it 'stats' extracted. The method will then use the locatorBases and
     * rootPath to construct all the relevant 'file' information and then will
     * attached it to the incoming <code>ResourceBuilder</code> as a property named
     * 'file'.
     *
     * @param fileToParse     This is the <code>File</code> that will be inspected
     * @param locatorBases    This is the <code>String []</code> of locator bases
     *                        that will be used to construct correct paths
     * @param rootPath        this is the rootPath that points to the same location
     *                        as the locator bases so that relative paths can be
     *                        constructed properly.
     * @param resourceBuilder is the <code>ResourceBuilder</code> that will have all
     *                        the file 'stats' attached to it
     */
    private void extractFileStats(File fileToParse, String[] locatorBases, String rootPath,
            Resource.Builder resourceBuilder) {

        logger.debug("Going to collect file stats for file {}", fileToParse.getAbsolutePath());

        // Start date for overall extraction
        Date extractStartDate = new Date();

        // Some start and end dates for fine grained logging of action durations
        Date start = new Date();
        Date end;

        logger.debug("Starting file stat extraction at {}", start);

        // Generate the path relative to the root directory
        String relativePath = fileToParse.getAbsolutePath().substring(rootPath.length());
        logger.debug("Path relative to root {}: {}", rootPath, relativePath);

        // We can first create the top level array of locators
        for (int i = 0; i < locatorBases.length; i++) {
            // Create a locator for each base
            String locator = locatorBases[i] + relativePath;
            if (locator != null) {
                // And add it to the Resource builder
                logger.debug("Locator: {}", locator);
                resourceBuilder.addLocators(locator);
            }
        }

        // Grab the path of the parent folder
        String parentFolder = fileToParse.getParent();
        logger.debug("Parent folder: {}", parentFolder);

        // MD5 hash it so we can easily get a listing of files in the parent folder
        String hashedRoot = null;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(parentFolder.getBytes());
            byte[] parentFolderMD5Bytes = messageDigest.digest();
            hashedRoot = DatatypeConverter.printHexBinary(parentFolderMD5Bytes);
        } catch (NoSuchAlgorithmException e) {
            logger.error("NoSuchAlgorithmException caught trying to create hash of parent folder: {}", e.getMessage());
        }

        // Create path object and add it to the Resource builder
        org.mbari.model.Path path = org.mbari.model.Path.newBuilder().setVirtual(relativePath)
                .setReal(fileToParse.getAbsolutePath()).setUri(fileToParse.toURI().toString()).setRoot(hashedRoot)
                .build();

        resourceBuilder.setPath(path);

        // Create the file builder
        org.mbari.model.File.Builder fileBuilder = org.mbari.model.File.newBuilder();

        // If it's a file, calculate and set the CRC
        start = new Date();
        if (!fileToParse.isDirectory()) {
            Long crcOfFile = null;
            try (InputStream fileInputStream = new BufferedInputStream(new FileInputStream(fileToParse))) {
                byte[] buf = new byte[1024 * 64];
                // A local variable to hold latest read
                int gByte;
                while ((gByte = fileInputStream.read(buf)) > 0) {
                    crc32.update(buf, 0, gByte);
                }
                fileInputStream.close();

                // Grab the value
                crcOfFile = crc32.getValue();
            } catch (IOException e) {
                logger.warn("IOException caught trying to calculate MD5 hash for file {}", fileToParse.getName());
                logger.warn(e.getMessage());
            }

            // Add it to the File
            if (crcOfFile != null) {
                // Add it to the file builder
                fileBuilder.setCrc(crcOfFile);
            }
        }
        end = new Date();
        logger.debug("CRC32 calculation took {} milliseconds", (end.getTime() - start.getTime()));
        resourceBuilder.addProcessingMessages(
                String.format("CRC32 calculation took {} milliseconds", (end.getTime() - start.getTime())));

        // Now, let's figure out the MIME type of the file
        String mimeType = null;
        if (fileToParse.isDirectory()) {
            mimeType = "inode/directory";
        } else {
            try {
                mimeType = tika.detect(fileToParse);
            } catch (IOException e) {
                logger.error("IOException trying to detect MIME Type using Tika: {}", e.getMessage());
            }
        }

        if (mimeType != null) {
            fileBuilder.setContentType(mimeType);
        } else {
            resourceBuilder.addProcessingMessages("Could not detect MIME Type");
            logger.debug("Could not detect MIME Type");
        }

        // Look up the extension of the file
        String extension = FilenameUtils.getExtension(fileToParse.getName());
        if (extension != null) {
            // Set it on the file builder
            fileBuilder.setExtension(extension);
        }

        // This is the name of the file without any path
        String filename = fileToParse.getName();
        if (filename != null) {
            // Set it on the file builder
            fileBuilder.setFileName(filename);
        }

        // Grab the basic file attributes
        BasicFileAttributes attrs = null;
        try {
            attrs = Files.readAttributes(fileToParse.toPath(), BasicFileAttributes.class);
        } catch (IOException e) {
            logger.error("IOException caught trying to get basic attributes from file {}",
                    fileToParse.getAbsolutePath());
            logger.error("Message: {}", e.getMessage());
        }

        // Make sure we have attributes and assign them if they are found
        if (attrs != null) {
            fileBuilder.setCreated(attrs.creationTime().toMillis());
            fileBuilder.setLastAccessed(attrs.lastAccessTime().toMillis());
            fileBuilder.setLastModified(attrs.lastModifiedTime().toMillis());
            fileBuilder.setFileSize(attrs.size());
        } else {
            resourceBuilder.addProcessingMessages(
                    "Could not get file attributes: created, lastAccessed, " + "lastModified, or size");
            logger.warn("Could not get file attributes for file {}", fileToParse.toPath());
        }

        // Add the URI relative to the host who extracted this info
        fileBuilder.setUri(fileToParse.toURI().toString());

        // Add the File object to the Resource builder
        resourceBuilder.setFile(fileBuilder.build());

        // A datetime when the file stat was finished
        Date extractEndDate = new Date();
        resourceBuilder.addProcessingMessages("It took " + (extractEndDate.getTime() - extractStartDate.getTime())
                + " milliseconds to extract file stats");
        logger.debug("{} milliseconds for file stat extraction.",
                (extractEndDate.getTime() - extractStartDate.getTime()));
    }

    /**
     * This method takes in a <code>String</code> that should represent a Date that
     * was parsed out by Tika and converts it to a <code>Long</code> which is epoch
     * milliseconds.
     *
     * @param metadataDateString
     * @return
     */
    private Long convertMetadataDateStringToEpochMillis(String metadataDateString) {
        // The timestamp to return
        Long epochMillis = null;

        // Convert string to date
        try {
            DateTime dateTime = new DateTime(metadataDateString);
            epochMillis = dateTime.toDate().getTime();
        } catch (Exception e) {
            logger.error("Exception caught trying to parse {} into a Date object: {}", metadataDateString,
                    e.getMessage());
        }

        // Return the result
        return epochMillis;
    }

    /**
     * This method takes in a <code>File</code> and if it is not a directory, it
     * uses Apache Tika (and other tools) to try and parse metadata properties as
     * well as text content from the file. If content was found, it goes through a
     * cleaning process of that content to make it compact and amenable to search.
     * It then attaches the metadata object to the incoming
     * <code>Resource.Builder</code> as a property 'metadata' and then content as a
     * property named 'content'.
     *
     * @param fileToParse     is the <code>File</code> that will be examined
     * @param resourceBuilder is the <code>Resource.Builder</code> that will have
     *                        the metadata and content added to.
     */
    private void extractMetadata(File fileToParse, Resource.Builder resourceBuilder) {

        logger.debug("extractMetadata called on file {}", fileToParse.getAbsolutePath());

        // Make sure this is not a directory
        if (!fileToParse.isDirectory()) {

            // Grab a start date to log performance for debugging
            Date extractTikaStatsStart = new Date();

            // Let's also create a Map that will hold all the name-value pairs that are
            // extracted
            // from different parsers
            Map<String, String> parsedMetadataMap = new HashMap<String, String>();

            // Create the Metadata builder
            org.mbari.model.Metadata.Builder metadataBuilder = org.mbari.model.Metadata.newBuilder();

            // Let's extract the mime type using Tika
            String mimeType = null;
            try {
                mimeType = tika.detect(fileToParse);
            } catch (IOException e) {
                logger.error("IOException caught trying to using tika.detect to get MIMEType: ");
                logger.error(e.getMessage());
            }

            // Let's do the Tika parsing first

            // Create the Tika auto detect parser
            AutoDetectParser autoDetectParser = new AutoDetectParser();

            // Create the Tika custom output stream
            ContentOutputStream contentOutputStream = new ContentOutputStream();

            // And a Tika body content handler
            BodyContentHandler bodyContentHandler = new BodyContentHandler(contentOutputStream);

            // Also create a Tika metadata object that will be populated by Tika parsing
            Metadata metadata = new Metadata();

            // Create a Tika parser context that is needed by the Tika parser
            ParseContext parseContext = new ParseContext();

            // Try to open a file input stream that the parser needs
            FileInputStream fileInputStream = null;
            try {
                fileInputStream = new FileInputStream(fileToParse);
            } catch (IOException e) {
                logger.error("IOException caught trying to open input stream from file {}", fileToParse.getName());
                logger.error(e.getMessage());
            }

            // If the input stream exists, parse it
            if (fileInputStream != null) {

                // Grab a timestamp before starting the Tika extraction
                Date start = new Date();

                // Now try to parse the file using Tika
                try {
                    autoDetectParser.parse(fileInputStream, bodyContentHandler, metadata, parseContext);
                } catch (IOException e) {
                    logger.error("IOException caught trying to use Tika to parse {}", fileToParse.getName());
                    logger.error(e.getMessage());
                    resourceBuilder.addProcessingMessages(
                            String.format("IOException trying to parse metadata from file: {}", e.getMessage()));
                } catch (SAXException e) {
                    logger.error("SAXException caught trying to use Tika to parse {}", fileToParse.getName());
                    logger.error(e.getMessage());
                    resourceBuilder.addProcessingMessages(
                            String.format("Error trying to parse metadata from file: {}", e.getMessage()));
                } catch (TikaException e) {
                    logger.error("TikaException caught trying to use Tika to parse {}", fileToParse.getName());
                    logger.error(e.getMessage());
                    resourceBuilder.addProcessingMessages(
                            String.format("TikaException trying to parse metadata from file: {}", e.getMessage()));
                    if (e.getCause() != null) {
                        logger.error(e.getCause().getMessage());
                        resourceBuilder.addProcessingMessages(String.format("Cause: {}", e.getCause().getMessage()));
                    }
                }

                // Now a timestamp after the metadata parsing
                Date end = new Date();
                logger.debug("Metadata parsing took {} milliseconds", (end.getTime() - start.getTime()));
                resourceBuilder.addProcessingMessages(
                        String.format("Metadata parsing took {} milliseconds", (end.getTime() - start.getTime())));

                // Now let's roll through metadata properties
                for (String name : metadata.names()) {

                    // Make sure the key is not in the metadata map already and add it
                    if (!parsedMetadataMap.containsKey(name))
                        parsedMetadataMap.put(name, metadata.get(name));

                }
            } // End Tika parsing

            // Now let's parse with ExifTool
            ExifTool exifTool = new ExifToolBuilder().build();
            Map<Tag, String> tags = null;
            try {
                tags = exifTool.getImageMeta(fileToParse);
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            if (tags != null) {
                for (Map.Entry<Tag, String> entry : tags.entrySet()) {
                    String entryKey = entry.getKey().getName();
                    String entryValue = entry.getValue();
                    if (!parsedMetadataMap.containsKey(entryKey))
                        parsedMetadataMap.put(entryKey, entryValue);
                }
            }

            // Now let's iterate over the parsed metadata
            for (Map.Entry<String, String> entry : parsedMetadataMap.entrySet()) {

                // Grab the property name
                String name = entry.getKey();
                String value = parsedMetadataMap.get(name);

                // Since Content-Type is important, look for that specifically
                if (name.equals("Content-Type")) {
                    // Check to see if we parsed a MIME Type earlier
                    if (mimeType != null) {
                        // Check to see if they are different and add a message to that effect
                        if (!metadata.get(name).equals(mimeType)) {
                            resourceBuilder.addProcessingMessages(
                                    "Tika parsed two different Content-Types: " + mimeType + " and " + value);
                        }
                        // We will use the mime type extracted earlier as the top level content type as
                        // it seemed to be more accurate
                        metadataBuilder.setContentType(mimeType);
                    } else {
                        // Use the content type found here since we did not find a mime type before
                        metadataBuilder.setContentType(value);
                    }
                }

                // Add it to the raw list of properties
                metadataBuilder.putRaw(name, value);

                // Look for an altitude property
                try {
                    if (name.equalsIgnoreCase("altitude") || name.equalsIgnoreCase("gpsaltitude"))
                        metadataBuilder.setAltitude(Float.parseFloat(value));
                } catch (NumberFormatException e) {
                    logger.warn(
                            String.format("NumberFormatException trying to set altitude of {} as an altitude", value));
                }

                // Check for an author
                if (name.equalsIgnoreCase("author"))
                    metadataBuilder.setAuthor(value);

                // Check to see if it is a comment
                if (name.equalsIgnoreCase("comments"))
                    metadataBuilder.addComments(value);

                // Look for contributor
                if (name.equalsIgnoreCase("contributor"))
                    metadataBuilder.setContributor(value);

                // Look for coverage
                if (name.equalsIgnoreCase("coverage"))
                    metadataBuilder.setCoverage(value);

                // Note that I don't look for coverage_spatial, coverage_temporal_end, or
                // coverage_temporal_start as that will not come from Tika

                // Look for created time in epoch millis
                if (name.equalsIgnoreCase("created")) {
                    // Convert to epoch millis
                    Long dateEpochMillis = convertMetadataDateStringToEpochMillis(value);
                    if (dateEpochMillis != null) {
                        // Set the epoch millis
                        metadataBuilder.setCreated(dateEpochMillis);
                    }
                }

                // Look for a creator tag
                if (name.equalsIgnoreCase("creator"))
                    metadataBuilder.setCreator(value);

                // Look for the tool that created it
                if (name.equalsIgnoreCase("creator_tool"))
                    metadataBuilder.setCreatorTool(value);

                // Now look for a date field
                if (name.equalsIgnoreCase("date")) {
                    // Convert to epoch millis if we can
                    Long dateEpochMillis = convertMetadataDateStringToEpochMillis(value);
                    if (dateEpochMillis != null) {
                        // Set the epoch millis
                        metadataBuilder.setDate(dateEpochMillis);
                    }
                }

                // Look for a description
                if (name.equalsIgnoreCase("description"))
                    metadataBuilder.setDescription(value);

                // Look for format tag
                if (name.equalsIgnoreCase("format"))
                    metadataBuilder.setFormat(value);

                // Look for identifier tag
                if (name.equalsIgnoreCase("identifier"))
                    metadataBuilder.addIdentifiers(value);

                // Check to see if it's a keyword
                if (name.equalsIgnoreCase("keywords"))
                    metadataBuilder.addKeywords(value);

                // Look for language
                if (name.equalsIgnoreCase("language"))
                    metadataBuilder.setLanguage(value);

                // Now look for latitude
                try {
                    if (name.equalsIgnoreCase("latitude") || name.equalsIgnoreCase("gpslatitude")) {
                        metadataBuilder.setLatitude(Float.parseFloat(value));
                    }
                } catch (NumberFormatException e) {
                    logger.warn(
                            String.format("NumberFormatException caught trying to convert {} to a latitude", value));
                }

                // Note I do not look for "location" here as you would not get that from Tika

                // Now look for longitude
                try {
                    if (name.equalsIgnoreCase("longitude") || name.equalsIgnoreCase("gpslongitude")) {
                        metadataBuilder.setLongitude(Float.parseFloat(value));
                    }
                } catch (NumberFormatException e) {
                    logger.warn(
                            String.format("NumberFormatException caught trying to convert {} to a longitude", value));
                }

                // Look for metadata date
                if (name.equalsIgnoreCase("metadata_date")) {
                    // Convert to epoch millis
                    Long dateEpochMillis = convertMetadataDateStringToEpochMillis(value);
                    if (dateEpochMillis != null) {
                        // Set the epoch millis
                        metadataBuilder.setMetadataDate(dateEpochMillis);
                    }
                }

                // Look for modified date
                if (name.equalsIgnoreCase("modified")) {
                    // Convert to epoch millis
                    Long dateEpochMillis = convertMetadataDateStringToEpochMillis(value);
                    if (dateEpochMillis != null) {
                        // Set the epoch millis
                        metadataBuilder.setModified(dateEpochMillis);
                    }
                }

                // Look for a modifier
                if (name.equalsIgnoreCase("modifier"))
                    metadataBuilder.setModifier(value);

                // Look for a print date
                if (name.equalsIgnoreCase("print_date")) {
                    // Convert to epoch millis
                    Long dateEpochMillis = convertMetadataDateStringToEpochMillis(value);
                    if (dateEpochMillis != null) {
                        // Set the epoch millis
                        metadataBuilder.setPrintDate(dateEpochMillis);
                    }
                }

                // Look for publisher
                if (name.equalsIgnoreCase("publisher"))
                    metadataBuilder.setPublisher(value);

                // Look for rating
                if (name.equalsIgnoreCase("rating"))
                    metadataBuilder.setRating(value);

                // Look for relation
                if (name.equalsIgnoreCase("relation"))
                    metadataBuilder.setRelation(value);

                // Look for right statement
                if (name.equalsIgnoreCase("rights"))
                    metadataBuilder.setRights(value);

                // Look for a source
                if (name.equalsIgnoreCase("source"))
                    metadataBuilder.setSource(value);

                // Look for a title
                if (name.equalsIgnoreCase("title"))
                    metadataBuilder.setTitle(value);

                // Look for a type
                if (name.equalsIgnoreCase("type"))
                    metadataBuilder.setType(value);
            }

            // Add the Metadata object to the Resource builder
            resourceBuilder.setMetadata(metadataBuilder.build());

            // Check for content and if there, go ahead and add it
            if (contentOutputStream.getContent() != null && contentOutputStream.getContent().length() > 0) {
                resourceBuilder.addProcessingMessages(
                        "Content is " + contentOutputStream.getContent().length() + " characters long");

                // Add the content to the Resource builder
                resourceBuilder.setContent(contentOutputStream.getContent());
            }

            // Create a end date to log how long this all took
            Date extractTikaStatsEnd = new Date();
            logger.debug("All metadata and content extraction took {} milliseconds",
                    (extractTikaStatsEnd.getTime() - extractTikaStatsStart.getTime()));
            resourceBuilder
                    .addProcessingMessages(String.format("All metadata and content extraction took {} milliseconds",
                            (extractTikaStatsEnd.getTime() - extractTikaStatsStart.getTime())));
        } else {
            logger.debug("It is a directory so no Tika stats will be extracted");
        }
    }

    /**
     * This method takes in a File object and if the file has a MIME Type that
     * indicates it has tree structure (like JSON or XML), that tree structure is
     * parsed into JSON and attached to the Resource as the 'content tree'
     *
     * @param fileToParse
     * @param resourceBuilder
     */
    private void extractContentTree(File fileToParse, Resource.Builder resourceBuilder) {

        // Make sure a file was passed in and it exists
        if (fileToParse != null && fileToParse.exists() && resourceBuilder.getMetadataBuilder() != null
                && resourceBuilder.getMetadataBuilder().getContentType() != null) {

            // Now that we know we have the content type, look for XML mime types
            if (resourceBuilder.getMetadataBuilder().getContentType().equals("application/xml")) {
                // Grab start date and time so we can log how long this takes
                Date extractContentTreeStart = new Date();

                // Read in the file into a string
                StringBuilder xmlBuilder = new StringBuilder();
                try (BufferedReader br = new BufferedReader(new FileReader(fileToParse))) {
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        xmlBuilder.append(line);
                    }
                } catch (IOException e) {
                    logger.error("IOException caught trying to read XML file to convert to JSON: {}", e.getMessage());
                    resourceBuilder.addProcessingMessages(
                            "IOException caught trying to read XML file to convert to JSON: " + e.getMessage());
                }

                // Now read in the XML string and convert to a JSONObject
                JSONObject contentTreeInJSON = XML.toJSONObject(xmlBuilder.toString());

                // Now convert JSON to string and attach to the resourceBuilder
                resourceBuilder.setContentTree(contentTreeInJSON.toString());

                // Now grab the end time
                Date extractContentTreeEnd = new Date();
                logger.debug("XML to JSON conversion took {} milliseconds",
                        (extractContentTreeEnd.getTime() - extractContentTreeStart.getTime()));
                resourceBuilder.addProcessingMessages("XML to JSON conversion took "
                        + (extractContentTreeEnd.getTime() - extractContentTreeStart.getTime()) + " milliseconds");
            }

            // And JSON file types
            if (resourceBuilder.getMetadataBuilder().getContentType().equals("application/json")) {
                // Read the file in using the JSON parser
                JSONObject jsonObject = RepositoryIndexerUtils.readJSONObjectFromFile(fileToParse.getAbsolutePath());
                // Add it to the resource builder as a string
                if (jsonObject != null) {
                    resourceBuilder.setContentTree(jsonObject.toString());
                }
            }
        }
    }

    /**
     * This method takes in a <code>File</code> and an associated
     * <code>Resource.Builder</code> and looks to see if a property named
     * 'content_type' exists either in the 'file' properties or in the 'metadata'
     * properties and then uses that content_type to determine if it might have
     * actual data in variables. Currently, if it's a comma-separated values file
     * (CSV) or a NetCDF file, it will try to extract variables and add them to a
     * 'data' property on the incoming <code>Resource.Builder</code>
     *
     * @param fileToParse     the <code>File</code> that will be examined to see if
     *                        it has variables containing data
     * @param resourceBuilder the <code>Resource.Builder</code> that contains the
     *                        necessary information to see if it might have
     *                        variables and is also the object that will have the
     *                        variable metadata attached to it as a 'data' property.
     */
    private void extractVariables(File fileToParse, Resource.Builder resourceBuilder) {

        Date start = new Date();

        // Let's check to see if there are file stats that we can use to figure out if
        // the file might have variables
        if (resourceBuilder.getFile() != null && resourceBuilder.getFile().getContentType() != null) {
            // Check for possible CSV file
            if (resourceBuilder.getFile().getContentType().equalsIgnoreCase("text/csv")) {
                resourceBuilder
                        .addProcessingMessages("Indexer sees this as a CSV file and will try to parse variables");
                extractVariablesFromCSV(fileToParse, resourceBuilder);
                return;
            }

            // Check for possible NetCDF file
            if (resourceBuilder.getFile().getContentType().equalsIgnoreCase("application/x-netcdf")
                    || resourceBuilder.getFile().getContentType().equalsIgnoreCase("application/x-hdf")) {
                resourceBuilder
                        .addProcessingMessages("Indexer sees this as a NetCDF file and will try to parse variables");
                extractVariablesFromNetCDF(fileToParse, resourceBuilder);
                return;
            }
        }

        // If we are here, we haven't found any variables yet, so let's see if there is
        // a 'metadata' object that we can
        // inspect for content type
        if (resourceBuilder.getMetadata() != null && resourceBuilder.getMetadata().getContentType() != null) {
            // Check for CSV
            if (resourceBuilder.getMetadata().getContentType().equalsIgnoreCase("text/csv")) {
                resourceBuilder
                        .addProcessingMessages("Indexer sees this as a CSV file and will try to parse variables");
                extractVariablesFromCSV(fileToParse, resourceBuilder);
                return;
            }

            // Check for NetCDF
            if (resourceBuilder.getMetadata().getContentType().equalsIgnoreCase("application/x-netcdf")
                    || resourceBuilder.getMetadata().getContentType().equalsIgnoreCase("application/x-hdf")) {
                resourceBuilder
                        .addProcessingMessages("Indexer sees this as a NetCDF file and will try to parse variables");
                extractVariablesFromNetCDF(fileToParse, resourceBuilder);
                return;
            }
        }
        Date end = new Date();
        logger.debug("Variable extraction took {} milliseconds", (end.getTime() - start.getTime()));
    }

    /**
     * This method attempts to extract variable metadata from a comma-separated
     * value (CSV) file and attach that metadata to 'data.variables' property on the
     * incoming <code>Resource.Builder</code>. NOTE: This method assumes there is
     * only one header line and that line has the variable information in it. Also,
     * it will try to find any parens in the header and if it finds one, it will
     * assume that the text in the parens are the units.
     *
     * @param fileToParse     the <code>File</code> that will be examined
     * @param resourceBuilder the <code>Resource.Builder</code> that will have the
     *                        variable metadata attached to as a 'data.variables'
     *                        property
     */
    private void extractVariablesFromCSV(File fileToParse, Resource.Builder resourceBuilder) {

        logger.debug("Attempting to extract variables from CSV file {}", fileToParse.getAbsolutePath());

        // Compile a pattern to look for units in parentheses
        Pattern unitsPattern = Pattern.compile("(.*)\\((.*)\\)");

        // Create the CSVFormat object
        CSVFormat csvFormat = CSVFormat.RFC4180.withFirstRecordAsHeader();

        // Create the file reader
        Reader in = null;
        try {
            in = new FileReader(fileToParse);
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException looking for {}", fileToParse);
        }

        // If the reader is OK
        if (in != null) {

            // Now parse the file into records
            CSVParser parser = null;
            try {
                parser = csvFormat.parse(in);
            } catch (IOException e) {
                logger.error("IOException caught trying to create CSV parser for {}", fileToParse);
                logger.error("Message:{} ", e.getMessage());
                resourceBuilder.addProcessingMessages(
                        "Thought this was a CSV file, but error occurred trying to " + "parse it: " + e.getMessage());
            }

            // If the parse seems OK
            if (parser != null) {

                // Grab the header map
                Map<String, Integer> headerMap = parser.getHeaderMap();

                // Now check to see if there is a "data" field yet on the JSONObject
                if (resourceBuilder.getData() == null || resourceBuilder.getData().getVariablesCount() <= 0) {

                    // Create a Data builder
                    Data.Builder dataBuilder = Data.newBuilder();

                    // Let's first create a HashMap that associates a variable with a Data.Variable
                    // so we can have a
                    // variable builder for each variable found in the header
                    HashMap<String, Data.Variable.Builder> variableBuilders = new HashMap<>();

                    // Create the builders and parse info from the column headers
                    for (String variableName : headerMap.keySet()) {

                        // Create a variable builder
                        Data.Variable.Builder variableBuilder = Data.Variable.newBuilder();

                        // Placeholders for name and units parsed from header
                        String name = variableName.trim();
                        String units = null;

                        // Search for any units that might be in parens
                        Matcher unitsMatcher = unitsPattern.matcher(variableName);
                        if (unitsMatcher.matches()) {
                            // Overwrite original name with one from pattern match
                            name = unitsMatcher.group(1).trim();

                            // Grab the units
                            units = unitsMatcher.group(2).trim();
                        }

                        // Add the name
                        variableBuilder.setShortName(name);

                        // If units were found, create an attribute with units
                        if (units != null && units.length() > 0) {
                            // Attribute builder
                            org.mbari.model.Attribute.Builder unitsAttributeBuilder = org.mbari.model.Attribute
                                    .newBuilder();
                            unitsAttributeBuilder.setShortName("units");
                            unitsAttributeBuilder.setDataType("string");
                            unitsAttributeBuilder.setValue(units);

                            // Add it to the variable
                            variableBuilder.addAttributes(unitsAttributeBuilder.build());
                        }

                        // Create an attribute for the column_index
                        org.mbari.model.Attribute.Builder colIndexAttributeBuilder = org.mbari.model.Attribute
                                .newBuilder();

                        colIndexAttributeBuilder.setShortName("column_index");
                        colIndexAttributeBuilder.setDataType("string");
                        colIndexAttributeBuilder.setValue(headerMap.get(variableName) + "");

                        // Add to the variable
                        variableBuilder.addAttributes(colIndexAttributeBuilder.build());
                        logger.debug("Created variable builder for {} with units {} and column index {}", variableName,
                                units, headerMap.get(variableName));

                        // Add it to the hashmap
                        variableBuilders.put(variableName.trim(), variableBuilder);
                    }

                    // This is another hashmap that associates variable name with data type
                    HashMap<String, String> dataTypeHashMap = new HashMap<>();

                    // OK, this is a bit unfortunate, but due to the fact that you can only roll
                    // through parser
                    // records once before having to create an entirely new parser, you need to roll
                    // through the
                    // records and look for variable data types across each record instead of going
                    // variable by
                    // variable. This is actually probably the most efficient way, but harder to
                    // code up ... sigh.
                    logger.debug("Going to loop over records to try to figure out data types");
                    for (CSVRecord record : parser) {

                        StringBuilder logBuilder = new StringBuilder();
                        logBuilder.append("Record " + record.getRecordNumber() + ":");

                        // Now iterate over each column header
                        for (String variableName : headerMap.keySet()) {

                            // Clean any spaces from around the column header
                            String name = variableName.trim();
                            logBuilder.append(variableName + "->");

                            // Check to see if the record has the variable
                            if (record.isSet(variableName)) {
                                // Since there is a value, try to guess what the data type is
                                String guessedDataType = getDataTypeFromString(record.get(variableName));
                                logBuilder.append(guessedDataType + ",");

                                // Grab the current data type from the hashmap
                                String currentDataType = dataTypeHashMap.get(name);

                                // If there was nothing in the map, just insert it
                                if (currentDataType == null) {
                                    dataTypeHashMap.put(name, guessedDataType);
                                } else {
                                    // If the guessed type is more specific, overwrite what's in the hashmap
                                    if (guessedDataType.equals("boolean") && currentDataType.equals("text")) {
                                        dataTypeHashMap.put(name, guessedDataType);
                                    }
                                    if (guessedDataType.equals("timestamp") && currentDataType.equals("text")) {
                                        dataTypeHashMap.put(name, guessedDataType);
                                    }
                                    if (guessedDataType.equals("bigint") && (!currentDataType.equals("float8"))) {
                                        dataTypeHashMap.put(name, guessedDataType);
                                    }
                                    if (guessedDataType.equals("float8")) {
                                        dataTypeHashMap.put(name, guessedDataType);
                                    }
                                }
                            } else {
                                logger.debug("Record {} does not seem to have a value for {}", record.getRecordNumber(),
                                        variableName);
                            }
                        }
                        logger.debug(logBuilder);

                        // If we have gone through 500 records, that should be enough
                        if (record.getRecordNumber() > 500)
                            break;
                    }

                    // Now I should have a hashmap with the variable names and their best guessed
                    // data types so let's
                    // update the variable builders with that information
                    for (String variableName : headerMap.keySet()) {
                        // Grab the data type
                        String dataType = dataTypeHashMap.get(variableName.trim());

                        // Grab the builder
                        Data.Variable.Builder variableBuilder = variableBuilders.get(variableName.trim());

                        // Make sure we actually have a data type
                        if (dataType != null) {
                            // Add the data type
                            logger.debug("Setting data type of variable {} to {}", variableName, dataType);
                            variableBuilder.setDataType(dataType);
                        }

                        // Now add the variable to the data builder
                        dataBuilder.addVariables(variableBuilder.build());
                    }

                    // Add data to resource builder
                    resourceBuilder.setData(dataBuilder.build());

                    logger.debug("Attached {} variables", resourceBuilder.getData().getVariablesCount());

                } else {
                    resourceBuilder.addProcessingMessages("While trying to add Variables from a CSV file ("
                            + fileToParse.toPath() + "), a Data object was already found, so we will skip "
                            + "trying to find data variables.");
                    logger.warn("While trying to add Variables from a CSV file ({}), a Data object was already "
                            + "found, so we will skip trying to find data variables.", fileToParse.toPath());
                }
            }
        }
    }

    /**
     * This method does it's best to figure out, with some major assumptions, what
     * data type is represented by the string value sent in. It tries to parse out
     * numbers first using a float as the most specific, then an integer. If that
     * does not work, it will then try to convert using an ISO formatted date and if
     * that doesn't work, it will just assume a string (text). While this is limited
     * in scope, it's a starting point
     *
     * @param value
     * @return
     */
    private String getDataTypeFromString(String value) {

        logger.debug("getDataTypeFromString called with value {}", value);

        // The type to return - default is text
        String typeToReturn = "text";

        // First make sure there is a value
        if (value != null && !value.equals("")) {

            // Before we pattern match, trim off any white space and capitalize in case
            // there is a lower case e
            String cappedAndTrimmedValue = value.trim().toUpperCase();

            // Now try to match to scientific notation
            Matcher sciNotationMatcher = sciNotationPattern.matcher(cappedAndTrimmedValue);
            if (sciNotationMatcher.matches()) {
                logger.debug("Matches scientific notation, will set type to float8");
                typeToReturn = "float8";
            } else {
                // OK, so not scientific notation, so let's try a float, this pattern only
                // matches decimal numbers
                Matcher decimalOnlyMatcher = decimalOnlyPattern.matcher(value.trim());
                if (decimalOnlyMatcher.matches()) {
                    logger.debug("Matches decimal, will set type to float8");
                    typeToReturn = "float8";
                } else {
                    // OK, so not sci notation and not float, we need to test for integers
                    Matcher integerOnlyMatcher = integerOnlyPattern.matcher(value.trim());
                    if (integerOnlyMatcher.matches()) {
                        logger.debug("Matches integer, will set type to bigint");
                        typeToReturn = "bigint";
                    } else {
                        // OK, so we are into the values being strings, but there are still
                        // possibilities here. We
                        // could have boolean or dates. Let's look for booleans first
                        if (cappedAndTrimmedValue.equals("TRUE") || cappedAndTrimmedValue.equals("FALSE")) {
                            logger.debug("Matches true or false, will set type to boolean");
                            typeToReturn = "boolean";
                        } else {
                            // Lastly, let's look for dates. Right now, ISO formatted dates is all I look
                            // for
                            DateTime dateTry = null;
                            try {
                                dateTry = new DateTime(value.trim());
                            } catch (Exception e) {
                                logger.debug("Exception caught trying to parse a value of {} to a date: {}", value,
                                        e.getMessage());
                            }
                            if (dateTry != null) {
                                logger.debug("Parse to date, will set type to timestamp");
                                typeToReturn = "timestamp";
                            } else {
                                // So, we are here and there is one other possibility. If all has failed to this
                                // point
                                // as a last ditch effort (instead of just making it a string) we will look for
                                // value
                                // that start with a decimal number and ignore the rest.
                                Matcher decimalAtStartMatcher = decimalAtStartPattern.matcher(value.trim());
                                if (decimalAtStartMatcher.matches()) {
                                    logger.debug("It looks like this might be a float based on the fact "
                                            + "it start with a decimal, going to assume it is");
                                    typeToReturn = "float8";
                                } else {
                                    logger.debug(
                                            "Nothing could be determined from the value {} will assume it's just a text field",
                                            value);
                                }
                            }
                        }
                    }
                }
            }
        }

        // Return the results
        logger.debug("Will return type of {}", typeToReturn);
        return typeToReturn;
    }

    /**
     * This is a helper method that takes in a <code>Message.Builder</code> and a
     * <code>List</code> of NetCDF <code>Attribute</code>s. It constructs
     * <code>org.mbari.model.Attributes</code> for each NetCDF attribute and add it
     * to the incoming builder. It check to see if the builder is a Data.Builder or
     * a Data.Variable.Builder so that it uses the correct method call to add the
     * attributes.
     *
     * @param builder    is the <code>Message.Builder</code> who will have the
     *                   <code>Attribute</code>s added to
     * @param attributes is the <code>List</code> of <code>Attributes</code> to add
     *                   to the incoming <code>JSONObject</code>
     */
    private void addAttributeListToBuilder(Message.Builder builder, List<Attribute> attributes) {

        // Now loop over the list of attributes
        for (Attribute attribute : attributes) {
            // Create an attribute builder
            org.mbari.model.Attribute.Builder attributeBuilder = org.mbari.model.Attribute.newBuilder();

            if (attribute.getShortName() != null) {
                attributeBuilder.setShortName(attribute.getShortName());
            }
            if (attribute.getFullName() != null) {
                attributeBuilder.setFullName(attribute.getFullName());
            }
            if (attribute.getDODSName() != null) {
                attributeBuilder.setDodsName(attribute.getDODSName());
            }
            if (attribute.getDataType() != null && attribute.getDataType().getPrimitiveClassType() != null
                    && attribute.getDataType().getPrimitiveClassType().getSimpleName() != null) {
                attributeBuilder
                        .setDataType(attribute.getDataType().getPrimitiveClassType().getSimpleName().toLowerCase());
            }

            // Add the value
            if (attribute.isString()) {
                if (attribute.getStringValue() != null) {
                    attributeBuilder.setValue(attribute.getStringValue());
                }
            } else {
                Number number = attribute.getNumericValue();
                if (number != null && !Double.isNaN(number.doubleValue())) {
                    attributeBuilder.setValue(number + "");
                } else {
                    attributeBuilder.setValue("Unknown");
                }
            }

            // There are two builders that have attributes, one is Data and one if Variable,
            // figure out which one
            if (builder instanceof Data.Builder) {
                Data.Builder dataBuilder = (Data.Builder) builder;
                // add the attribute
                dataBuilder.addAttributes(attributeBuilder.build());

            } else if (builder instanceof Data.Variable.Builder) {
                Data.Variable.Builder variableBuilder = (Data.Variable.Builder) builder;
                variableBuilder.addAttributes(attributeBuilder.build());
            }
        }
    }

    /**
     * This method uses the NetCDF library to extract <code>Attribute</code>s,
     * <code>Dimension</code>s, and <code>Variable</code>s from the file and attach
     * it to the 'data' property on the incoming <code>Resource.Builder</code>
     *
     * @param fileToParse     the file that should be a NetCDF file and will be
     *                        examined
     * @param resourceBuilder the <code>Resource.Builder</code> that will have
     *                        things added to it as the 'data' property
     */
    private void extractVariablesFromNetCDF(File fileToParse, Resource.Builder resourceBuilder) {

        // Try to open the file as a NetCDF file
        try (NetcdfFile netcdfFile = NetcdfFile.open(fileToParse.getAbsolutePath())) {

            // If the file was opened OK
            if (netcdfFile != null) {

                // Check to see if Resource builder has a Data object yet. If so, don't do
                // anything (this should not happen)
                if (resourceBuilder.getData() == null || resourceBuilder.getData().getVariablesCount() <= 0) {

                    // Create the new Data builder
                    Data.Builder dataBuilder = Data.newBuilder();

                    // Add global attributes to the Data.Builder
                    addAttributeListToBuilder(dataBuilder, netcdfFile.getGlobalAttributes());

                    // Create an array list of Attributes that we can add using information from
                    // file description
                    ArrayList<Attribute> fileAttributes = new ArrayList<>();

                    // Now grab any file information and if some is found, add it as an attribute
                    String fileTypeDescription = netcdfFile.getFileTypeDescription();
                    if (fileTypeDescription != null && fileTypeDescription.length() > 0) {
                        fileAttributes.add(new Attribute("file_type_description", fileTypeDescription));
                    }
                    String fileTypeId = netcdfFile.getFileTypeId();
                    if (fileTypeId != null && fileTypeId.length() > 0) {
                        fileAttributes.add(new Attribute("file_type_id", fileTypeId));
                    }
                    String fileTypeVersion = netcdfFile.getFileTypeVersion();
                    if (fileTypeVersion != null && fileTypeVersion.length() > 0) {
                        fileAttributes.add(new Attribute("file_type_version", fileTypeVersion));
                    }
                    String id = netcdfFile.getId();
                    if (id != null && id.length() > 0) {
                        fileAttributes.add(new Attribute("file_id", id));
                    }
                    String location = netcdfFile.getLocation();
                    if (location != null && location.length() > 0) {
                        fileAttributes.add(new Attribute("file_location", location));
                    }
                    String title = netcdfFile.getTitle();
                    if (title != null && title.length() > 0) {
                        fileAttributes.add(new Attribute("file_title", title));
                    }

                    // Now if some attributes were generated, add them to the top level
                    if (fileAttributes.size() > 0) {
                        // Convert to an array
                        addAttributeListToBuilder(dataBuilder, fileAttributes);
                    }

                    // Now let's add any dimensions
                    List<Dimension> dimensions = netcdfFile.getDimensions();

                    if (dimensions != null) {
                        for (Dimension dimension : dimensions) {
                            // Create the dimension builder
                            Data.Dimension.Builder dimensionBuilder = Data.Dimension.newBuilder();
                            if (dimension.getShortName() != null) {
                                dimensionBuilder.setShortName(dimension.getShortName());
                            }
                            if (dimension.getFullName() != null) {
                                dimensionBuilder.setFullName(dimension.getFullName());
                            }
                            if (dimension.getDODSName() != null) {
                                dimensionBuilder.setDodsName(dimension.getDODSName());
                            }
                            dimensionBuilder.setLength(dimension.getLength());
                            dimensionBuilder.setIsUnlimited(dimension.isUnlimited());
                            dimensionBuilder.setIsShared(dimension.isShared());
                            dimensionBuilder.setIsVariableLength(dimension.isVariableLength());

                            // Add it to the data builder
                            dataBuilder.addDimensions(dimensionBuilder.build());
                        }
                    }

                    // Now let's add any variables
                    List<Variable> variables = netcdfFile.getVariables();
                    if (variables != null) {
                        for (Variable variable : variables) {

                            // Create the Variable builder
                            Data.Variable.Builder variableBuilder = Data.Variable.newBuilder();

                            // Create the array of attributes
                            addAttributeListToBuilder(variableBuilder, variable.getAttributes());

                            // Add variable information
                            if (variable.getShortName() != null) {
                                variableBuilder.setShortName(variable.getShortName());
                            }
                            if (variable.getFullName() != null) {
                                variableBuilder.setFullName(variable.getFullName());
                            }
                            if (variable.getDODSName() != null) {
                                variableBuilder.setDodsName(variable.getDODSName());
                            }
                            if (variable.getDataType() != null && variable.getDataType().getPrimitiveClassType() != null
                                    && variable.getDataType().getPrimitiveClassType().getSimpleName() != null) {
                                variableBuilder.setDataType(
                                        variable.getDataType().getPrimitiveClassType().getSimpleName().toLowerCase());
                            }
                            if (variable.getDescription() != null) {
                                variableBuilder.setDescription(variable.getDescription());
                            }
                            variableBuilder.setIsCoordinateVariable(variable.isCoordinateVariable());
                            variableBuilder.setIsVariableLength(variable.isVariableLength());
                            variableBuilder.setIsUnlimited(variable.isUnlimited());
                            variableBuilder.setRank(variable.getRank());
                            variableBuilder.setSize(variable.getSize());
                            if (variable.getUnitsString() != null) {
                                variableBuilder.setUnitsString(variable.getUnitsString());
                            }

                            List<Dimension> variableDimensions = variable.getDimensions();
                            if (variableDimensions != null) {
                                for (Dimension variableDimension : variableDimensions) {
                                    variableBuilder.addDimensionShortNames(variableDimension.getShortName());
                                }
                            }

                            // Add the variable
                            dataBuilder.addVariables(variableBuilder.build());
                        }
                    }

                    resourceBuilder.setData(dataBuilder.build());
                    logger.debug("Attached {} variables, {} dimensions, and {} global attributes from the NetCDF file",
                            dataBuilder.getVariablesCount(), dataBuilder.getDimensionsCount(),
                            (netcdfFile.getGlobalAttributes().size() + fileAttributes.size()));

                } else {
                    resourceBuilder.addProcessingMessages(
                            "While trying to add Variables from a NetCDF file (" + fileToParse.toPath()
                                    + "), a Data object was already found, so we will skip adding " + "variables.");
                    logger.warn(
                            "While trying to add Variables from a NetCDF file ({}), "
                                    + "a Data object was already found, so we will skip adding variables.",
                            fileToParse.toPath());
                }
            }
        } catch (IOException e) {
            logger.error("IOException trying to open the NetCDF file {}", fileToParse.getName());
            logger.error(e.getMessage());
            resourceBuilder.addProcessingMessages(
                    "There was an error trying to open this file as a NetCDF file: " + e.getMessage());
        }

    }
}

/**
 * This class is a custom output stream that can be used to handle content
 * coming from the Tika Content handler. I created this to create very
 * streamlined content for indexing process.
 */
class ContentOutputStream extends OutputStream {

    // This is an array list of strings that will be used to hold all the tokens
    // that are parsed
    ArrayList<String> tokenList = new ArrayList<>();

    // This is a string builder to build up each individual token
    StringBuilder tokenBuilder = new StringBuilder();

    // The default contructor
    public ContentOutputStream() {
    }

    @Override
    public void write(int b) {
        int[] bytes = { b };
        write(bytes, 0, bytes.length);
    }

    // This is the method that will be called by the Tika content handler while it
    // is parsing the content.
    public void write(int[] bytes, int offset, int length) {

        // Try to create a string from the incoming byte array
        String s = null;
        try {
            s = new String(bytes, offset, length);

            // Remove any non alpha numeric characters
            s = s.replaceAll("[^a-zA-Z_]", "");

            // If there is something there, append it to the token builder
            if (s.length() > 0)
                tokenBuilder.append(s);
        } catch (Exception e) {
            // Do nothing as this will happen while parsing binary files
        }

        // If nothing from the incoming byte array was converted to a string, that
        // probably means a token break so
        // go ahead and write the token to the array list and start a new token builder
        if (s == null || s.length() <= 0) {

            // Convert the token builder to a string
            String token = tokenBuilder.toString();

            // Make sure it's long enough to even care about
            if (token.length() > 2 && !tokenList.contains(token)) {
                tokenList.add(token);
            }

            // Now, start a new token builder
            tokenBuilder = new StringBuilder();
        }
    }

    // This method converts the array list into a string and returns it.
    public String getContent() {
        StringBuilder contentBuilder = new StringBuilder();
        for (String token : tokenList) {
            contentBuilder.append(token + " ");
        }
        return contentBuilder.toString();
    }
}
