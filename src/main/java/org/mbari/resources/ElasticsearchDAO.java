package org.mbari.resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Request;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.mbari.model.Resource;

/**
 * This class provides access methods to an Elasticsearch database so that
 * database logic is abstracted away from other classes.
 */
public class ElasticsearchDAO {

    // The logger
    private static Logger logger = LogManager.getLogger(ElasticsearchDAO.class);

    // Some contants
    static final String SHORT_NAME = "short_name";
    static final String METADATA = "metadata";
    static final String RAW_FLAT = "rawFlat";
    static final String CONTENT_TREE = "content_tree";
    static final String LOCATORS = "locators";

    // The elasticsearch connection properties
    private String elasticsearchHost = null;
    private Integer elasticsearchPort = null;
    private String elasticsearchUsername = null;
    private String elasticsearchPassword = null;

    // The client connection
    private RestHighLevelClient client = null;

    // The ProtocolBuffer util to convert PB objects to JSON
    private JsonFormat.Printer protocolBuffersPrinter = JsonFormat.printer().preservingProtoFieldNames();

    /**
     * The constructor that sets up the connection to the Elasticsearch database
     *
     * @param elasticsearchHost     the name of the host that is running
     *                              Elasticsearch
     * @param elasticsearchPort     the port that Elasticsearch is listening to
     * @param elasticsearchUsername the username that should be used to connect to
     *                              Elasticsearch
     * @param elasticsearchPassword the password to be used when connecting to
     *                              Elasticsearch
     */
    public ElasticsearchDAO(String elasticsearchHost, Integer elasticsearchPort, String elasticsearchUsername,
            String elasticsearchPassword) {

        logger.info("Connecting to Elasticsearch using: host: {}, port: {}, username: {}", elasticsearchHost,
                elasticsearchPort, elasticsearchUsername);

        // Store the connection information locally.
        this.elasticsearchHost = elasticsearchHost;
        this.elasticsearchPort = elasticsearchPort;
        this.elasticsearchUsername = elasticsearchUsername;
        this.elasticsearchPassword = elasticsearchPassword;

        // In order to use credentials, create a provider
        final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY,
                new UsernamePasswordCredentials(this.elasticsearchUsername, this.elasticsearchPassword));

        // Now create a client builder
        RestClientBuilder builder = RestClient.builder(new HttpHost(this.elasticsearchHost, this.elasticsearchPort))
                .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
                    @Override
                    public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                        return httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
                    }
                });

        // And the client
        client = new RestHighLevelClient(builder);
    }

    /**
     * This method takes in a <code>String</code> that is trying to be used as an
     * index name in Elasticsearch. Elasticsearch has restrictions on things that
     * can be used as index names so this method converts the incoming name into
     * something that will work as an Elasticsearch index name.
     *
     * @param indexName The index name to convert
     * @return the converted name
     * @throws IllegalArgumentException if the incoming name can't be converted
     */
    public static String convertToValidIndexName(String indexName) {

        // The regular expression to look for invalid characters in an index name
        String invalidIndexCharsPatternString = "[~`!@#$%^&*()+={}\\[\\]\\\\|;:'\"<>,./?\\s+]";

        // If it's a '.' or a '..' just throw an exception, nothing can be done
        if (indexName.equals(".") || indexName.equals("..")) {
            logger.error("Could not convert incoming index name of {} to a valid Elasticsearch"
                    + " index name, so throwing an exception", indexName);
            throw new IllegalArgumentException("An index name of '.' or '..' is not allowed");
        }
        logger.debug("Going to convert the following index name to something elasticsearch can use");
        logger.debug(indexName);

        // The index name to return
        String validIndexName = indexName;

        // First convert everything to lowercase
        validIndexName = validIndexName.toLowerCase();

        // Replace any special chars with underscores, remove duplicate underscores and
        // remove any starting underscore
        validIndexName = validIndexName.replaceAll(invalidIndexCharsPatternString, "_").replaceAll("_{2,}", "_")
                .replaceAll("^_", "");

        // Now remove any starting dashes, underscores or pluses
        while (validIndexName.startsWith("-") || validIndexName.startsWith("_") || validIndexName.startsWith("+")) {
            validIndexName = validIndexName.substring(1);
        }

        // Now shorten the name if it's longer than 255 bytes
        byte[] validIndexNameAsBytes = validIndexName.getBytes();
        if (validIndexNameAsBytes.length > 255) {
            validIndexName = new String(Arrays.copyOfRange(validIndexNameAsBytes, 0, 254));
        }

        if (validIndexName.equals(indexName)) {
            logger.debug("No changes were necessary");
        } else {
            logger.debug("After conversion, valid index name is:");
            logger.debug(validIndexName);
        }

        // Now return the cleaned up name
        return validIndexName;
    }

    /**
     * This method takes in a <code>String</code> and checks to see if Elasticsearch
     * has an index with this name already.
     *
     * @param indexName the name to look for
     * @return true if an index with that name exists, false if not
     */
    private boolean indexExists(String indexName) {
        // The indicator flag to return
        boolean indexExists = false;

        // Grab the array of index names
        String[] indexes = this.getIndexList();

        // Now search
        for (String index : indexes) {
            if (index.equals(indexName)) {
                indexExists = true;
                break;
            }
        }

        // Return the flag
        return indexExists;
    }

    /**
     * This method returns an array of index names that exist in the Elasticsearch
     * database
     *
     * @return and array of <code>String</code>s that are the names of the indexes
     *         already in Elasticsearch
     */
    private String[] getIndexList() {

        // The ArrayList to hold the index names
        ArrayList<String> indexNames = new ArrayList<>();

        // Create the request to ask for the list of indexes
        Request indexListRequest = new Request("GET", "/_aliases");

        // Send the request for the index list
        Response response = null;
        try {
            // Do the query
            response = client.getLowLevelClient().performRequest(indexListRequest);
        } catch (IOException e) {
            logger.error("IOException caught searching for index names: {}", e.getMessage());
        } catch (Exception e) {
            logger.error("Unknown exception caught trying to search for index names: {}", e.getMessage());
        }

        // Grab the response
        JSONObject jsonResponse = null;
        if (response != null) {
            try {
                jsonResponse = new JSONObject(EntityUtils.toString(response.getEntity()));
            } catch (IOException e) {
                logger.error("IOException caught trying to parse response into JSON object: {}", e.getMessage());
            }
        }

        // Now try to pull the index names out of the JSONObject
        if (jsonResponse != null) {
            // Grab the keyset which should be the index names
            Set<String> keys = jsonResponse.keySet();
            for (String index : keys) {
                indexNames.add(index);
            }
        }
        // Now return the array
        return indexNames.toArray(new String[indexNames.size()]);
    }

    /**
     * This method takes in a <code>String</code> which is the name of a JSON
     * formatted file that should have a valid Elasticsearch mapping definition in
     * it. The method will search the classpath for a file by that name and will
     * read in the file as a string and pass it to the
     * <code>createIndexWithMapping</code> function.
     *
     * @param indexName    the name to use when creating the index in Elasticsearch
     * @param jsonFileName the name of the file to search for (on the classpath)
     */
    private void createIndexFromJSONFile(String indexName, String jsonFileName) {
        // Create a StringBuilder to build the string that will be used as a mapping
        // JSON object to create an index
        StringBuilder sb = new StringBuilder();

        // Create a buffered reader to read in the file.
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(getClass().getClassLoader().getResourceAsStream(jsonFileName)));) {

            String line;
            while ((line = br.readLine()) != null) {
                if (sb.length() > 0) {
                    sb.append("\n");
                }
                sb.append(line);
            }
        } catch (IOException e) {
            logger.error("IOException caught trying to read JSON file {}", jsonFileName);
        }

        // Call the method to create the index with that string
        createIndexWithMapping(indexName, sb.toString());
    }

    /**
     * This is a method that will check to see if an index with the given name
     * exists, and if it does not, will create it using the JSON string provided.
     *
     * @param indexName   the name of the index to create
     * @param jsonMapping the <code>String</code> that should be a JSON formatted
     *                    string that contains a valid Elasticsearch mapping
     * @throws IllegalArgumentException if the index name or json mapping are not
     *                                  provided or if the index can't be created
     *                                  using that mapping string
     */
    private void createIndexWithMapping(String indexName, String jsonMapping) {

        // Let's first verify wer have an index name
        if (indexName == null || indexName.equals("")) {
            logger.error("No index name provided, so no index could be created");
            throw new IllegalArgumentException("No index name provided, so no index could be created");
        }

        // And verify something is there for mapping string
        if (jsonMapping == null || jsonMapping.equals("")) {
            logger.error("No mapping JSON string provided, so no index could be created");
            throw new IllegalArgumentException("No mapping JSON string provided, so no index could be created");
        }

        // Now proceed
        logger.debug("Will create index of name {} if it does not already exist", indexName);

        // First check to see if the index exists
        if (!indexExists(indexName)) {

            // If the json mapping exists, create a string entry for it
            HttpEntity entity = new NStringEntity(jsonMapping, ContentType.APPLICATION_JSON);

            // Create the request to ask for the list of indexes
            Request createIndexRequest = new Request("PUT", "/" + indexName);
            createIndexRequest.setEntity(entity);

            // Now make the request
            Response response = null;
            try {
                response = client.getLowLevelClient().performRequest(createIndexRequest);
            } catch (IOException e) {
                logger.error("IOException caught trying to create the index: {}", e.getMessage());
                throw new IllegalArgumentException("Index was not created, likely due to invalid JSON mapping string. "
                        + "Error: " + e.getMessage());
            }

            if (response == null) {
                logger.error("Could not create index for some reason");
            } else {
                logger.debug("Index {} created", indexName);
            }
        } else {
            logger.warn("Index already exists, nothing done");
            throw new IllegalArgumentException("Index named " + indexName + " already exists");
        }
    }

    /**
     * This method takes in two <code>JSONArray</code>s and does it's best to copy
     * the values from the source to the target array. It does look for special
     * conditions in case the object in the array are <code>JSONObject</code>s. If
     * that is the case, it will look for 'short_name' as a key in the target array
     * to make sure it is updating the correct <code>JSONObject</code>. This method
     * is a bit tricky and makes some assumptions to prevent duplicates from leaking
     * in to the target.
     *
     * @param source the source <code>JSONArray</code>
     * @param target the target <code>JSONArray</code>
     */
    private void updateJSONArrays(JSONArray source, JSONArray target) {
        // Iterate over the source array
        for (Object sourceObject : source) {
            // Check for JSONArray or JSONObject or just assign to the target
            if (sourceObject instanceof JSONArray) {
                // This one is a problem. If the element of the array is another array, there
                // isn't much we can do to know which element in the target array is the one to
                // recursively call this with. For now, I will just log a warning

                // TODO kgomes - should we add it to the target array, just to make sure we have
                // it (get duplicates)?
                logger.warn("In the updateJSONArrays function, the elements in the array are JSONArrays and there is"
                        + "no way to know which element to match that with in the target, so for now, nothing is done");
            } else if (sourceObject instanceof JSONObject) {
                // For this to work, there needs to be a concept of an identifier property on
                // the source and target objects. I think for all cases we would deal with,
                // there is a property named 'short_name' that will serve that purpose, so let's
                // see if there is one on the source object
                String shortName = ((JSONObject) sourceObject).getString(SHORT_NAME);
                JSONObject targetJSONObject = null;
                if (shortName != null && !shortName.equals("")) {
                    // Now let's look loop over all the JSONObjects in the target array and see if
                    // one has a short
                    for (Object targetObject : target) {
                        if (targetObject instanceof JSONObject && ((JSONObject) targetObject).has(SHORT_NAME)
                                && ((JSONObject) targetObject).getString(SHORT_NAME).equals(shortName)) {
                            targetJSONObject = (JSONObject) targetObject;
                            break;
                        }
                    }

                    // If none was found, create a new one and add it
                    if (targetJSONObject == null) {
                        targetJSONObject = new JSONObject();
                        target.put(targetJSONObject);
                    }
                }

                // Now if we have both JSONObjects, update the target with the source
                if (targetJSONObject != null) {
                    updateJSONObject((JSONObject) sourceObject, targetJSONObject);
                }
            } else {
                // For this, we do not want to create duplicates, so I am going to try and take
                // the string value
                // of the source object, iterate over the target array and see if there is
                // anything in there that
                // has the same string value and if it does, skip it, otherwise, add it
                String sourceString = sourceObject.toString();

                boolean addToTarget = true;
                for (Object targetObject : target) {
                    if (targetObject.toString().equals(sourceString)) {
                        addToTarget = false;
                        break;
                    }
                }
                if (addToTarget) {
                    target.put(sourceObject);
                }
            }
        }
    }

    /**
     * This method takes in a source <code>JSONObject</code> and a target
     * <code>JSONObject</code> and does it's best to make sure the information in
     * the source ends up in the target. It does makes some assumptions, for example
     * if there is a property named 'messages', this method assumes it should just
     * replace the messages on the target as indexing messages should only be the
     * latest ones.
     *
     * @param source the source <code>JSONObject</code>
     * @param target the target <code>JSONObject</code>
     */
    private void updateJSONObject(JSONObject source, JSONObject target) {
        logger.debug("Updating target JSONObject from source for object");

        // Grab the top level keys
        Set<String> keys = source.keySet();

        // Iterate over they keys
        for (String key : keys) {
            logger.debug("Updating key: {}", key);

            // Grab the object associated with the key
            Object value = source.get(key);

            // Check to see if the value is a JSONArray
            if (value instanceof JSONArray) {
                // If the key name is "processing_messages" for the array, I am going to assume
                // this contains messages
                // from indexing and should just replace the old one
                if (key.equals("processing_messages")) {
                    // Create a new JSONArray for the target
                    JSONArray newTargetArray = new JSONArray();
                    // Iterate over the messages
                    for (Object message : (JSONArray) value) {
                        newTargetArray.put(message);
                    }
                    target.put(key, newTargetArray);
                } else {
                    // See if the target has an array with the same key
                    JSONArray targetJSONArray = null;
                    if (target.has(key)) {
                        // Grab the object
                        Object targetValue = target.get(key);

                        // Make sure it's a JSON array
                        if (targetValue instanceof JSONArray) {
                            targetJSONArray = (JSONArray) targetValue;
                        } else {
                            logger.warn("Trying to update key {}, which is a JSONArray, but the "
                                    + "target object has a property named {} but it is not a JSONArray, "
                                    + "nothing will be updated", key, key);
                        }
                    } else {
                        // Since the target did not have anything with that key name, create a new array
                        // and attach it
                        targetJSONArray = new JSONArray();
                        target.put(key, targetJSONArray);
                    }
                    // Now sync them
                    if (targetJSONArray != null) {
                        updateJSONArrays((JSONArray) value, targetJSONArray);
                    }
                }
            } else if (value instanceof JSONObject) {
                // Let's look for a JSONObject with the same key on the target
                JSONObject targetJSONObject = null;
                if (target.has(key)) {
                    // Grab the object
                    Object targetValue = target.get(key);
                    if (targetValue instanceof JSONObject) {
                        targetJSONObject = (JSONObject) targetValue;
                    } else {
                        logger.warn("Trying to update key {}, which is a JSONObject, but the "
                                + "target object has a property named {} but it is not a JSONObject, "
                                + "nothing will be updated", key, key);
                    }
                } else {
                    // Since target did not have a JSONObject with that name, create a new one and
                    // attach it
                    targetJSONObject = new JSONObject();
                    target.put(key, targetJSONObject);
                }
                if (targetJSONObject != null) {
                    updateJSONObject((JSONObject) value, targetJSONObject);
                }
            } else {
                target.put(key, value);
            }
        }

    }

    /**
     * This method sends a ping to the ES server to see if it's ready to handle
     * requests
     *
     * @return
     */
    public boolean isServerReady() {
        // Set the flag to not ready by default
        boolean serverReady = false;

        // Try to ping the server
        try {
            serverReady = client.ping(RequestOptions.DEFAULT);
        } catch (Exception e) {
            logger.warn("Exception caught trying to ping Elasticsearch server: {}", e.getMessage());
        }

        // Return the result
        return serverReady;
    }

    /**
     * This method returns the number of documents that are in the index with the
     * given name.
     *
     * @param indexName the index to count documents in
     * @return the number of documents in the index
     */
    public long getDocumentCount(String indexName) {
        // The number to return
        long documentCount = 0;

        // Convert the incoming index name into something that is valid
        String validIndexName = convertToValidIndexName(indexName);

        // First make sure repo exists
        if (indexExists(validIndexName)) {

            // Create the search request against the repo
            SearchRequest searchRequest = new SearchRequest(validIndexName);
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            sourceBuilder.query(QueryBuilders.matchAllQuery());

            // Now run the request
            try {
                SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
                documentCount = searchResponse.getHits().getTotalHits();
            } catch (IOException e) {
                logger.error("IOException searching for all document URIs in index {}", validIndexName);
                logger.error(e.getMessage());
            }
        }

        // Return the result
        return documentCount;
    }

    /**
     * This method looks up and returns all the file.uri properties for all the
     * document in the index with the given name. It also returns the UUID with the
     * URI string so the caller can link which document it came from. The method
     * takes a couple other parameters so the results can be retrieved in chunks.
     *
     * @param indexName the name of the index to count document in
     * @param from      this is the starting point of the results to return. This is
     *                  done so the returns can be done in chunks.
     * @param size      this is the number of results to return start at the from
     *                  number. Essentially this is the number of elements in the
     *                  returned <code>JSONArray</code> (unless there aren't enough
     *                  results to fill it to that size).
     * @return is a <code>JSONArray</code> that contains <code>JSONObjects</code>
     *         that have two properties. 1) 'uuid' which is the UUID of the
     *         document, and 2) 'file.uri' which is the file-based URI for the file
     *         associated with the document
     */
    public JSONArray getAllURIsFromIndex(String indexName, Integer from, Integer size) {
        // Grab parameters
        Integer localFrom = from;
        Integer localSize = size;
        // If the from and size parameters are null, use defaults of 0 and 10
        // respectively
        if (localFrom == null)
            localFrom = Integer.valueOf(0);
        if (localSize == null)
            localSize = Integer.valueOf(10);

        // The JSONArray to return
        JSONArray jsonArray = new JSONArray();

        // Convert the incoming index name into something that is valid
        String validIndexName = convertToValidIndexName(indexName);

        // First make sure repo exists
        if (indexExists(validIndexName)) {

            // We need to get all the documents in the index, but only return their ID and
            // file.uri

            // Create the search request against the repo
            SearchRequest searchRequest = new SearchRequest(validIndexName);
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            sourceBuilder.from(localFrom);
            sourceBuilder.size(localSize);
            sourceBuilder.query(QueryBuilders.matchAllQuery());

            // Specify the fields to return
            String[] includeFields = new String[] { "uuid", "file.uri" };
            sourceBuilder.fetchSource(includeFields, null);

            // Now attach source parameters
            searchRequest.source(sourceBuilder);

            // Now run the request
            SearchResponse searchResponse = null;
            try {
                searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
                SearchHits hits = searchResponse.getHits();
                SearchHit[] searchHits = hits.getHits();

                // Iterate over the results
                for (SearchHit hit : searchHits) {
                    // Create a new JSONObject
                    JSONObject jsonObject = new JSONObject();
                    // Grab the ID of the document
                    String id = hit.getId();
                    jsonObject.put("uuid", id);
                    // Now see if there is a URI on the file object
                    if (hit.getSourceAsMap() != null && hit.getSourceAsMap().get("file") != null) {
                        // Grab the source as map
                        Map<String, Object> sourceMap = hit.getSourceAsMap();
                        if (sourceMap != null && sourceMap.get("file") != null) {
                            Object fileObject = sourceMap.get("file");
                            // Now make sure it's a HashMap
                            if (fileObject instanceof Map) {
                                @SuppressWarnings("unchecked")
                                Map<String, Object> fileMap = (Map<String, Object>) fileObject;

                                // Now grab the URI
                                if (fileMap.get("uri") != null) {
                                    jsonObject.put("uri", fileMap.get("uri").toString());
                                }
                            }
                        }
                    }

                    // Add it to the array
                    jsonArray.put(jsonObject);
                }
            } catch (IOException e) {
                logger.error("IOException searching for all document URIs in index {}", validIndexName);
                logger.error(e.getMessage());
            }
        }

        // Return the results
        return jsonArray;
    }

    /**
     * This is a helper method that takes in a <code>JSONObject</code> and if the
     * object has a property named 'locators' that have a value of a
     * <code>JSONArray</code>, it converts that <code>JSONArray</code> to an array
     * of <code>String</code>s.
     *
     * @param resourceObject is the <code>JSONObject</code> that should have a
     *                       property named 'locators'
     * @return an array of <code>String</code>s that should be locators
     */
    private String[] getLocatorsFromResourceJSONObject(JSONObject resourceObject) {

        // Create and ArrayList to populate
        List<String> locatorsList = new ArrayList<String>();

        // Grab the JSON array from the JSON object
        if (resourceObject.has(LOCATORS)) {
            // Grab the JSONArray
            JSONArray locatorsJSONArray = resourceObject.getJSONArray(LOCATORS);
            for (int i = 0; i < locatorsJSONArray.length(); i++) {
                locatorsList.add(locatorsJSONArray.getString(i));
            }
        }

        // Now convert the array list to an array and return it
        return locatorsList.toArray(new String[locatorsList.size()]);
    }

    /**
     * This method returns a <code>JSONObject</code> that contains the file stat
     * information for the document that matches one of the locators in the given
     * array of locator URIs. It will return the first match found (in case there is
     * more than one, which actually should not happen)
     *
     * @param indexName is the name of the Elasticsearch index to perform the search
     *                  in
     * @param locators  is an array of <code>String</code>s that are locators to
     *                  search for
     * @return the <code>JSONObject</code> that has the 'file' stat information from
     *         the matching document along with a property that is the uuid of the
     *         document.
     */
    public JSONObject getFileStatsFromLocators(String indexName, String[] locators) {

        // Make sure the index name is valid and well formatted
        String validIndexName = convertToValidIndexName(indexName);

        // First create a JSONObject to return
        JSONObject jsonObject = null;

        // First make sure repo exists and we have locators to look at
        if (indexExists(validIndexName) && locators != null && locators.length > 0) {

            // So, we have to search for a document that matches any of the locators given
            // in the locators array. So,
            // let's iterate over them
            for (String locator : locators) {
                // Create the search request against the repo
                SearchRequest searchRequest = new SearchRequest(validIndexName);
                SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
                sourceBuilder.query(new TermQueryBuilder("locators", locator));

                // Specify the fields to return
                String[] includeFields = new String[] { "uuid", "file" };
                sourceBuilder.fetchSource(includeFields, null);

                // Now attach source parameters
                searchRequest.source(sourceBuilder);

                // Now run the request
                SearchResponse searchResponse = null;
                try {
                    searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
                    SearchHits hits = searchResponse.getHits();
                    SearchHit[] searchHits = hits.getHits();

                    // Make sure there are results
                    if (searchHits.length > 0) {

                        // Grab the first one
                        SearchHit hit = searchHits[0];

                        // Create the JSON object using the response
                        jsonObject = new JSONObject(hit.getSourceAsString());
                        break;
                    }
                } catch (IOException e) {
                    logger.error("IOException looking for document with locator: {}", locator);
                    logger.error(e.getMessage());
                }

            }
        }

        // Return the result
        return jsonObject;
    }

    /**
     * This method returns a <code>Resource</code> that matches the document in
     * Elasticsearch that was found in the index with the given name and that has
     * the same UUID.
     *
     * @param indexName the name of the Elasticsearch index to search
     * @param uuid      the UUID of the document to search for
     * @return a <code>Resource</code> with all the matching document info (or null
     *         if not found)
     * @throws IllegalArgumentException if indexName is not there or is not found or
     *                                  if uuid is not specified
     */
    public Resource getResourceByUUID(String indexName, String uuid) {

        // The resource to return
        Resource resourceToReturn = null;

        // Make sure we have an indexName and a UUID
        if (indexName == null || indexName.equals("")) {
            logger.error("indexName not provided in getResourceByUUID call");
            throw new IllegalArgumentException("indexName not provided in getResourceByUUID call");
        }
        if (uuid == null || uuid.equals("")) {
            logger.error("uuid not provide in getResourceByUUID call");
            throw new IllegalArgumentException("uuid not provide in getResourceByUUID call");
        }

        // Copy the index name
        String validIndexName = convertToValidIndexName(indexName);
        logger.debug("Will search index {} for resource with UUID {}", validIndexName, uuid);

        // Create a Resource.Builder to use to construct Resource object
        Resource.Builder resourceBuilder = Resource.newBuilder();

        // First make sure repo exists
        if (indexExists(validIndexName)) {
            // Construct the search query using the UUID
            SearchRequest searchRequest = new SearchRequest(validIndexName);
            SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
            sourceBuilder.query(new TermQueryBuilder("uuid", uuid));

            // Now attach source parameters
            searchRequest.source(sourceBuilder);

            // Now run the request
            SearchResponse searchResponse = null;
            try {
                searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
                SearchHits hits = searchResponse.getHits();
                SearchHit[] searchHits = hits.getHits();

                // Make sure there are results
                if (searchHits.length > 0) {
                    // Convert the first response to a Resource
                    JsonFormat.parser().merge(searchHits[0].getSourceAsString(), resourceBuilder);
                    resourceToReturn = resourceBuilder.build();
                }
            } catch (IOException e) {
                logger.error("IOException looking for document with _id: {}", uuid);
                logger.error(e.getMessage());
            }

        } else {
            throw new IllegalArgumentException(String.format("The index named {} does not exist", indexName));
        }

        // Return the result
        return resourceToReturn;
    }

    /**
     * This method returns a <code>Resource</code> that contains the document
     * information from a document that has a locator that matches any one of the
     * locators in the incoming array.
     *
     * @param indexName the name of the index to search
     * @param locators  the array of <code>String</code>s that are locators that are
     *                  used in the search
     * @return the <code>Resource</code> that contains the information from the
     *         document found by using the locators provided
     * @throws IllegalArgumentException if indexName is not there or is not found
     */
    public Resource getResourceByLocators(String indexName, String[] locators) throws IllegalArgumentException {

        // The resource to return
        Resource resourceToReturn = null;

        // Make sure we have an indexName
        if (indexName == null || indexName.equals("")) {
            logger.error("indexName not provided in getResourceByLocators call");
            throw new IllegalArgumentException("indexName not provided in getResourceByLocators call");
        }

        // Copy the index name
        String validIndexName = convertToValidIndexName(indexName);

        // The Resource.Builder
        Resource.Builder resourceBuilder = Resource.newBuilder();

        // First make sure repo exists
        if (indexExists(validIndexName)) {
            // So, we have to search for a document that matches any of the locators given
            // in the locators array. So, let's
            // iterate over them
            for (String locator : locators) {
                // Create the search request against the repo
                SearchRequest searchRequest = new SearchRequest(validIndexName);
                SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
                sourceBuilder.query(new TermQueryBuilder("locators", locator));

                // Now attach source parameters
                searchRequest.source(sourceBuilder);

                // Now run the request
                SearchResponse searchResponse = null;
                try {
                    searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
                    SearchHits hits = searchResponse.getHits();
                    SearchHit[] searchHits = hits.getHits();

                    // Make sure there are results
                    if (searchHits.length > 0) {
                        // Grab the first one
                        SearchHit hit = searchHits[0];

                        // Merge it with the ResourceBuilder
                        JsonFormat.parser().merge(hit.getSourceAsString(), resourceBuilder);

                        // And convert it to the Resource to be returned
                        resourceToReturn = resourceBuilder.build();

                        // And break out of the loop
                        break;
                    }
                } catch (IOException e) {
                    logger.error("IOException looking for document with locator: {}", locator);
                    logger.error(e.getMessage());
                }

            }
        } else {
            throw new IllegalArgumentException(String.format("The index named {} does not exist", indexName));
        }

        // Return the result
        return resourceToReturn;
    }

    /**
     * This method takes in an index name and a <code>Resource</code> object. The
     * method will look at the index specified and see if there is an existing
     * Resource document (either by UUID if specified, or by one of the locators in
     * the Resource). If one does not exist, it will be created and if one does
     * exist, the document will be updated using any differences in the incoming
     * document. TODO kgomes - default behavior is to update only those things that
     * the incoming resourceObject has, but we may want a flag to entirely replace
     * the existing document
     *
     * @param indexName the name of the index to put the document into
     * @param resource  the <code>Resource</code> to be indexed into Elasticsearch
     * @param uuid      the UUID to use as the ID. You don't have to specify this
     *                  and one will be generated if you don't specify one.
     * @return a <code>String</code> which is the UUID of the matching indexed
     *         document
     * @throws IllegalArgumentException if indexName is not there or is not found
     */
    public String indexResource(String indexName, Resource resource, String uuid) throws IllegalArgumentException {

        // Make sure we have an indexName
        if (indexName == null || indexName.equals("")) {
            logger.error("indexName not provided in indexResource call");
            throw new IllegalArgumentException("indexName not provided in indexResource call");
        }

        // Create a placeholder for the UUID to return (the document ID)
        String idToReturn = null;

        // Make sure incoming resource is there
        if (resource != null) {

            // Set the resource to be indexed
            JSONObject jsonObjectToIndex = null;

            // Grab the valid index name
            String validIndexName = convertToValidIndexName(indexName);

            // First check to see if an index exists with the name that is the same as the
            // indexName and if it does not,
            // create it.
            if (!indexExists(validIndexName)) {
                createIndexFromJSONFile(validIndexName, "es-resource.json");
                logger.debug("Index should be created now");
            }

            // The placeholder for a Resource from Elasticsearch that should match the
            // incoming resource
            Resource existingResource = null;

            // First look and see if there is a UUID on the incoming resource, search for an
            // existing by that one
            if (resource.getUuid() != null && !resource.getUuid().equals("")) {
                existingResource = getResourceByUUID(validIndexName, resource.getUuid());
            }

            // If no existing resource found and there is an incoming UUID, try to find one
            // using that
            if (existingResource == null && uuid != null && !uuid.equals("")) {
                existingResource = getResourceByUUID(validIndexName, uuid);
            }

            // If the existing resource is still null, try to find it by locators on the
            // incoming resource
            if (existingResource == null && resource.getLocatorsCount() > 0) {

                // Convert to a string array
                String[] locatorArray = new String[resource.getLocatorsCount()];
                locatorArray = resource.getLocatorsList().toArray(locatorArray);

                // And look for it
                existingResource = getResourceByLocators(validIndexName, locatorArray);
            }

            // See if there is an existing resource
            if (existingResource != null) {

                // Grab the ID of the existing resource
                idToReturn = existingResource.getUuid();

                try {
                    // Grab both the incoming and existing resources as JSONObjects
                    JSONObject resourceJSONObject = new JSONObject(protocolBuffersPrinter.print(resource));
                    JSONObject existingResourceJSONObject = new JSONObject(
                            protocolBuffersPrinter.print(existingResource));

                    // Update the existing resource with the incoming one
                    updateJSONObject(resourceJSONObject, existingResourceJSONObject);

                    // Now set the JSONObject to index as the existing one
                    jsonObjectToIndex = existingResourceJSONObject;

                } catch (InvalidProtocolBufferException e) {
                    logger.error("InvalideProtocolBufferException trying to convert a Resource to a JSON object: {}",
                            e.getMessage());
                }

            } else {
                // Create a whole new resource, assign a UUID if not one already
                if (idToReturn == null) {
                    idToReturn = UUID.randomUUID().toString();
                }

                // Convert the incoming resource to a JSONObject
                try {
                    jsonObjectToIndex = new JSONObject(protocolBuffersPrinter.print(resource));
                } catch (InvalidProtocolBufferException e) {
                    logger.error(
                            "InvalidProtocolBufferException caught trying to convert incoming Resource to a JSONObject: {}",
                            e.getMessage());
                }

                // Set the ID
                if (jsonObjectToIndex != null) {
                    jsonObjectToIndex.put("uuid", idToReturn);
                }
            }

            // Make sure we have a JSON Object before continuing on
            if (jsonObjectToIndex != null) {
                // Next, we need to examine the special case of the raw metadata that can be
                // attached to the resource.
                // If the resource has a JSONObject attached at the 'raw' property, then we need
                // to make sure that
                // the raw values, get processed and attached as flattened data so we can enable
                // searching on the
                // hierarchical values in the raw metadata.
                if (jsonObjectToIndex.has(METADATA) && jsonObjectToIndex.getJSONObject(METADATA).has("raw")
                        && !jsonObjectToIndex.getJSONObject(METADATA).has(RAW_FLAT)) {
                    // If we are here, we know the metadata has a raw JSONObject, let's check to see
                    // if the flattened
                    // data is also there. If it's there, we won't do anything (for now)
                    // TODO kgomes - there should be some merging logic here to examine the raw
                    // object and make sure
                    // all the fields are in the flattened values. Be aware though, that the
                    // flattened values are not
                    // returned with a query, so this might be tough.
                    // Grab the raw JSON Object and try to flatten it
                    Map<String, Object> rawFlattened = ResourceIndexerUtils
                            .flattenObject(jsonObjectToIndex.getJSONObject(METADATA).getJSONObject("raw"));

                    // Make sure we got a result from the flattening
                    if (rawFlattened != null) {
                        logger.debug("Going to attach a flattened raw metadata object");
                        // Create a new JSONArray
                        JSONArray rawFlatArray = new JSONArray();

                        // Iterate over the keys
                        for (Map.Entry<String, Object> entry : rawFlattened.entrySet()) {
                            // Grab the key and value
                            String key = entry.getKey();
                            Object value = entry.getValue();
                            logger.debug("Key: {}, value: {}", key, value);
                            // Grab the flattened object and add it to the array
                            rawFlatArray.put(ResourceIndexerUtils.convertKeyValueToFlatObject(key, value));
                        }

                        // Add it
                        jsonObjectToIndex.getJSONObject(METADATA).put(RAW_FLAT, rawFlatArray);
                    }
                }

                // Secondly, there is another special case, where the content is hierarchical
                // and thus there will be
                // a 'content_tree' property. Let's first check to see if the content_tree is
                // there
                if (jsonObjectToIndex.has(CONTENT_TREE)) {
                    // OK, so we have a content_tree, but if it's coming from the raw parsing of a
                    // file, it will just
                    // be a string representation of the JSON content, so we need to see if it's a
                    // string first
                    if (jsonObjectToIndex.get(CONTENT_TREE) instanceof String) {
                        // OK, so we most likely have a JSON string representation of the content tree
                        // hierarchy. The
                        // object that we will store the actual JSON object has a raw property as well
                        // as a rawFlat
                        // property where the flattened version of the JSON object is placed
                        JSONObject contentTreeJSONObject = new JSONObject();
                        try {
                            // Parse the JSON string into a JSONObject
                            JSONObject rawContentTreeObject = new JSONObject(jsonObjectToIndex.getString(CONTENT_TREE));

                            // Now attach that object as the raw content tree object
                            contentTreeJSONObject.put("raw", rawContentTreeObject);

                            // Now let's flatten the content tree
                            Map<String, Object> contentTreeMap = ResourceIndexerUtils
                                    .flattenObject(rawContentTreeObject);

                            // If something came back, let's add the flattened data to a JSONArray
                            if (contentTreeMap != null) {
                                JSONArray contentTreeArray = new JSONArray();
                                for (Map.Entry<String, Object> entry : contentTreeMap.entrySet()) {
                                    // Grab the key and value
                                    String key = entry.getKey();
                                    Object value = entry.getValue();
                                    logger.debug("Key: {}, value: {}", key, value);
                                    // Grab the flattened object and add it to the array
                                    contentTreeArray.put(ResourceIndexerUtils.convertKeyValueToFlatObject(key, value));
                                }
                                // Now add it to the content tree object as rawFlat
                                contentTreeJSONObject.put(RAW_FLAT, contentTreeArray);
                            }

                            // Now attach the content_tree object where the string was before
                            jsonObjectToIndex.put(CONTENT_TREE, contentTreeJSONObject);
                        } catch (JSONException e) {
                            logger.error("JSONException trying to create content tree from string: {}", e.getMessage());
                        }

                    } else {
                        // This means it's most likely already an object and some kind of merge probably
                        // needs to be done
                        // TODO kgomes - think through this case
                    }
                }

                // Create a new index request
                IndexRequest request = new IndexRequest(validIndexName, "resource", idToReturn);

                // Set the JSON
                request.source(jsonObjectToIndex.toString(), XContentType.JSON);

                // Now index the resource
                IndexResponse indexResponse = null;
                try {
                    indexResponse = client.index(request, RequestOptions.DEFAULT);
                } catch (IOException e) {
                    logger.error("IOException caught trying to POST a new Resource to elasticsearch: {}",
                            e.getMessage());
                }

                // TODO kgomes - this is where a message needs to be pushed out that a
                // document/resource was indexed

                // If response was OK, make sure the ID matches that which is going to be
                // returned
                if (indexResponse != null && indexResponse.getId() != null
                        && !indexResponse.getId().equals(idToReturn)) {
                    logger.warn("Upon indexing a resource, the ID should have been {} but in the "
                            + "index response, the ID was listed as {}", idToReturn, indexResponse.getId());
                }
            }
        } else {
            logger.warn("indexResource was called, but no resource was provided");
        }

        // Now return the ID
        return idToReturn;
    }

    /**
     * This is a method that will remove a document from the index with the given
     * name that has the UUID provided.
     *
     * @param indexName the index to remove the document from
     * @param uuid      the UUID of the document to remove
     * @return a boolean to indicate if it was deleted or not
     * @throws IllegalArgumentException if indexName is not there or is not found or
     *                                  if UUID is not provided
     */
    public boolean removeResourceByUUID(String indexName, String uuid) {

        // The boolean to return
        boolean successful = false;

        // Make sure we have an indexName and a UUID
        if (indexName == null || indexName.equals("")) {
            logger.error("No indexName was provided");
            throw new IllegalArgumentException("No indexName was provided");
        }
        if (uuid == null || uuid.equals("")) {
            logger.error("No UUID was provided");
            throw new IllegalArgumentException("No UUID was provided");
        }

        // Convert the index name to a valid one
        String validIndexName = convertToValidIndexName(indexName);

        // Make sure it exists
        if (indexExists(validIndexName)) {
            // Create a new Delete request
            DeleteRequest deleteRequest = new DeleteRequest(indexName, "resource", uuid);

            // Now delete it
            try {
                client.delete(deleteRequest, RequestOptions.DEFAULT);
                successful = true;
            } catch (IOException e) {
                logger.error("IOException caught trying to delete from repo {} a document with UUID {}", indexName,
                        uuid);
                logger.error(e.getMessage());
            }
        } else {
            logger.warn("Something was trying to remove a document from index {} but the index does not exist",
                    indexName);
            throw new IllegalArgumentException("The index " + indexName + " does not exist");
        }

        // Return the resul
        return successful;
    }
}
