package org.mbari.resources;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

/**
 * The main purpose of this class is to just be the launcher for both a FileCrawler and an FSMonEventConsumer if the
 * environment is configured to run both.  It will first look for a config.json file on the classpath that will serve
 * to configure what the RepositoryIndexer is supposed to do.  It will then look for any environment variables that are
 * specified that will override properties in the config.json file.  Based on what it finds, it will configure and
 * launch a FileCrawler, a FSMonEventConsumer, or both.
 */
public class RepositoryIndexer {

    // The Logger
    private static Logger logger = LogManager.getLogger(RepositoryIndexer.class);

    // Constant definitions
    static final String ELASTICSEARCH_KEY = "elasticsearch";
    static final String HOST_KEY = "host";
    static final String PORT_KEY = "port";
    static final String USERNAME_KEY = "username";
    static final String PASSWORD_KEY = "password";
    static final String DATA_REPO_BASE_URL_KEY = "dataRepoBaseURL";
    static final String FILE_CRAWLER_KEY = "fileCrawler";
    static final String INTERVAL_BETWEEN_REPO_CRAWLS_IN_MINUTES_KEY = "intervalBetweenRepoCrawlsInMinutes";
    static final String DATA_REPO_KEY = "dataRepo";
    static final String NETWORK_MOUNT_KEY = "networkMount";
    static final String REPO_NAME_KEY = "repoName";
    static final String FS_MON_EVENT_CONSUMER_KEY = "fsMonEventConsumer";
    static final String RABBITMQ_KEY = "rabbitmq";
    static final String RABBITMQ_USERNAME_KEY = "username";
    static final String RABBITMQ_PASSWORD_KEY = "password";
    static final String RABBITMQ_HOST_KEY = "host";
    static final String RABBITMQ_PORT_KEY = "port";
    static final String RABBITMQ_VHOST_KEY = "vhost";
    static final String RABBITMQ_EXCHANGE_NAME_KEY = "exchangeName";
    static final String RABBITMQ_ROUTING_KEY_KEY = "routingKey";

    // This is the Elasticsearch interface object
    private ElasticsearchDAO elasticsearchDAO = null;

    // This is the class that is used to index resources
    private ResourceIndexer resourceIndexer = null;

    // The FileCrawler
    private FileCrawler fileCrawler = null;

    // The event consumer
    private FSMonEventConsumer fsMonEventConsumer = null;

    /**
     * This is the constructor for the class
     */
    public RepositoryIndexer() {

        logger.debug("Creating RepositoryIndexer object");
        
        // First, let's try to read a config.json file from the classpath somewhere
        JSONObject configObject = RepositoryIndexerUtils.readJSONObjectFromFile("/config.json");

        // If no JSONObject was returned, create an empty one
        if (configObject == null) configObject = new JSONObject();

        // Now load any environment variables that are being used to override configs read from the file
        RepositoryIndexerUtils.loadPropertiesFromEnvironment(configObject);

        // Validate the configuration
        validateConfigurationObject(configObject);

        // Grab the Elasticsearch connection configuration
        JSONObject esConfig = configObject.getJSONObject(ELASTICSEARCH_KEY);

        // Now create the Elasticsearch DAO
        elasticsearchDAO = new ElasticsearchDAO(esConfig.getString(HOST_KEY), esConfig.getInt(PORT_KEY),
                esConfig.getString(USERNAME_KEY), esConfig.getString(PASSWORD_KEY));

        // Now the ResourceIndexer
        this.resourceIndexer = new ResourceIndexer(this.elasticsearchDAO);

        // Create an array of locator bases
        String[] locatorBaseURIs = {configObject.getString(DATA_REPO_BASE_URL_KEY)};

        // Check to see if a FileCrawler is to be instantiated and if so, start one up
        if (configObject.has(FILE_CRAWLER_KEY) && configObject.getBoolean(FILE_CRAWLER_KEY)) {
            fileCrawler = new FileCrawler(elasticsearchDAO, resourceIndexer,
                    configObject.getInt(INTERVAL_BETWEEN_REPO_CRAWLS_IN_MINUTES_KEY),
                    configObject.getString(DATA_REPO_KEY), configObject.getBoolean(NETWORK_MOUNT_KEY),
                    locatorBaseURIs, configObject.getString(REPO_NAME_KEY));
        }

        // Check for FSMonEventConsumer start up and if yes, start one up
        if (configObject.has(FS_MON_EVENT_CONSUMER_KEY) && configObject.getBoolean(FS_MON_EVENT_CONSUMER_KEY)) {
            // Grab the rabbitmq config object
            JSONObject rmqConfig = configObject.getJSONObject(RABBITMQ_KEY);

            // Now create the event consumer
            fsMonEventConsumer = new FSMonEventConsumer(elasticsearchDAO, resourceIndexer,
                    configObject.getString(DATA_REPO_KEY), configObject.getString(REPO_NAME_KEY),
                    configObject.getString(DATA_REPO_BASE_URL_KEY), configObject.getBoolean(NETWORK_MOUNT_KEY),
                    rmqConfig.getString(RABBITMQ_HOST_KEY), rmqConfig.getInt(RABBITMQ_PORT_KEY), rmqConfig.getString(RABBITMQ_VHOST_KEY),
                    rmqConfig.getString(RABBITMQ_USERNAME_KEY), rmqConfig.getString(RABBITMQ_PASSWORD_KEY),
                    rmqConfig.getString(RABBITMQ_EXCHANGE_NAME_KEY), rmqConfig.getString(RABBITMQ_ROUTING_KEY_KEY));
        }
    }


    /**
     * This method takes in a JSONObject that should contain the configuration properties and will throw exceptions if
     * something is not correct
     *
     * @param jsonObject
     */
    private static void validateConfigurationObject(JSONObject jsonObject) {

        // First let's make sure there is a flag to indicate if a FileCrawler should be launched, if not, set one to
        // true since it's the default
        if (!jsonObject.has(FILE_CRAWLER_KEY)) jsonObject.put(FILE_CRAWLER_KEY, true);

        // Same for the FSMonEventConsumer
        if (!jsonObject.has(FS_MON_EVENT_CONSUMER_KEY)) jsonObject.put(FS_MON_EVENT_CONSUMER_KEY, true);

        // Make sure the dataRepo is defined and that it exists locally
        if (!jsonObject.has(DATA_REPO_KEY)) {
            throw new IllegalArgumentException("'dataRepo' property was not found in a config.json and " +
                    "FTESI_DATA_REPO was not found in the environment, this is required to run this application");
        }

        // Make sure the repoName exists first
        if (!jsonObject.has(REPO_NAME_KEY)) {
            throw new IllegalArgumentException("The 'repoName' property was not found in a config.json and " +
                    "FTESI_REPO_NAME was not found in the environment, this is required to run this application");
        } else {
            // Run the repoName through the validator for ES index names
            String validatedRepoName = ElasticsearchDAO.convertToValidIndexName(jsonObject.getString(REPO_NAME_KEY));

            // If it's different than the one supplied, throw an exception and make a suggestion
            if (!validatedRepoName.equals(jsonObject.getString(REPO_NAME_KEY))) {
                throw new IllegalArgumentException("It appears the repoName that you supplied (" +
                        jsonObject.getString(REPO_NAME_KEY) +
                        ") will not work as an index for Elasticsearch, may we suggest " + validatedRepoName +
                        " instead?");
            }
        }

        // Now look for the network mount flag and if not there, set to false
        if (!jsonObject.has(NETWORK_MOUNT_KEY)) {
            jsonObject.put(NETWORK_MOUNT_KEY, false);
        }

        // Now, what about the base URL where the repo can be found
        if (!jsonObject.has(DATA_REPO_BASE_URL_KEY)) {
            throw new IllegalArgumentException("The 'dataRepoBaseURL' property was not found in the config.json and " +
                    "FTESI_DATA_REPO_BASE_URL was not found in the environment variables.  This is required.");
        }

        // Check to see if an interval between file crawls is specified and if not, choose a default of 5 minutes
        if (!jsonObject.has(INTERVAL_BETWEEN_REPO_CRAWLS_IN_MINUTES_KEY)) {
            jsonObject.put(INTERVAL_BETWEEN_REPO_CRAWLS_IN_MINUTES_KEY, 5);
        }

        // If a FileCrawler or FSMonEventConsumer are enabled, check to make sure we have all the properties needed to
        // connect to the Elasticsearch server
        if (jsonObject.getBoolean(FILE_CRAWLER_KEY) || jsonObject.getBoolean(FS_MON_EVENT_CONSUMER_KEY)) {
            // Make sure there is an elasticsearch object
            if (!jsonObject.has(ELASTICSEARCH_KEY)) {
                throw new IllegalArgumentException("It does not appear you have configured any Elasticsearch " +
                        "information in either your config.json or your environment.  This is required.");
            }

            // If we are here, we should have an elasticsearch object
            JSONObject esObject = jsonObject.getJSONObject(ELASTICSEARCH_KEY);

            // Now make sure we have the server name
            if (!esObject.has(HOST_KEY)) {
                throw new IllegalArgumentException("Did not find a hostname where Elasticsearch is running and " +
                        "this is required.");
            }

            // Look for the elasicsearch port and if not specified, use 9200
            if (!esObject.has(PORT_KEY)) {
                esObject.put("port", 9200);
            }

            // Now make sure we have the username and if not assign the default
            if (!esObject.has(USERNAME_KEY)) {
                esObject.put(USERNAME_KEY, "elastic");
            }

            // Same for the password
            if (!esObject.has(PASSWORD_KEY)) {
                esObject.put(PASSWORD_KEY, "changeme");
            }
        }

        // Now if a FSMonEventConsumer is enabled, make sure we have the RabbitMQ properties
        if (jsonObject.getBoolean(FS_MON_EVENT_CONSUMER_KEY)) {

            // Make sure there is a rabbitmq object first
            if (!jsonObject.has(RABBITMQ_KEY)) {
                throw new IllegalArgumentException("It does not appear you have configured any RabbitMQ information " +
                        "in either your config.json or your environment, this is necessary for a event consumer");
            }

            // Grab the object so we can check the properties
            JSONObject rmqObject = jsonObject.getJSONObject(RABBITMQ_KEY);

            // check for the hostname
            if (!rmqObject.has(RABBITMQ_HOST_KEY)) {
                throw new IllegalArgumentException("No RabbitMQ host name was found in the config.json and the " +
                        "FTESI_RABBITMQ_HOST was not found in the environment, these are required");
            }

            // Look for the port and if not specified, use 5672
            if (!rmqObject.has(RABBITMQ_PORT_KEY)) {
                rmqObject.put(RABBITMQ_PORT_KEY, 5672);
            }

            // Now make sure we have the username
            if (!rmqObject.has(RABBITMQ_USERNAME_KEY)) {
                throw new IllegalArgumentException("The username for the RabbitMQ connection was not specified in " +
                        "either the config.json file or as FTESI_RABBIMQ_USERNAME environment variable");
            }

            // Same for the password
            if (!rmqObject.has(RABBITMQ_PASSWORD_KEY)) {
                throw new IllegalArgumentException("The password for the RabbitMQ connection was not specified in " +
                        "either the config.json file or as FTESI_RABBIMQ_PASSWORD environment variable");
            }

            // Same for the vhost
            if (!rmqObject.has(RABBITMQ_VHOST_KEY)) {
                throw new IllegalArgumentException("The vhost for the RabbitMQ connection was not specified in " +
                        "either the config.json file or as FTESI_RABBIMQ_VHOST environment variable");
            }

            // Same for the exhange name
            if (!rmqObject.has(RABBITMQ_EXCHANGE_NAME_KEY)) {
                throw new IllegalArgumentException("The exchangeName for the RabbitMQ connection was not specified in " +
                        "either the config.json file or as FTESI_RABBIMQ_EXCHANGE_NAME environment variable");
            }

            // Look for routing key and if one is not found assign the default
            if (!rmqObject.has(RABBITMQ_ROUTING_KEY_KEY)) {
                rmqObject.put(RABBITMQ_ROUTING_KEY_KEY, "*");
            }
        }
    }

    private void startIndexing() {
        // TODO kgomes: make sure ES connection is alive
        if (fileCrawler != null) {
            fileCrawler.start();
        }
        if (fsMonEventConsumer != null) {
            fsMonEventConsumer.start();
        }
    }


    /**
     * This is the main function that fires everything off
     *
     * @param args
     */
    public static void main(String[] args) {

        // Create the RepositoryIndexer
        RepositoryIndexer repositoryIndexer = new RepositoryIndexer();

        // Start the indexing
        repositoryIndexer.startIndexing();
    }
}
