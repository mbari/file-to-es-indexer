package org.mbari.resources;

import com.github.wnameless.json.flattener.JsonFlattener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.util.Map;

/**
 * This class provides some static helper methods that can be used during the
 * indexing of documents
 */
public class ResourceIndexerUtils {

    // Create a logger
    private static Logger logger = LogManager.getLogger(RepositoryIndexerUtils.class);

    /**
     * This method takes in a string and does it's best to figure out what data type
     * is contained in the string. It will examine the string and return one of the
     * following: 1. 'boolean' 2. 'long' 3. 'double' 4. 'date' 5. 'string' 6. 'null'
     *
     * @param value
     * @return
     */
    public static String getDataTypeFromString(String value) {
        logger.debug("getDataTypeFromString called with value {}", value);

        // Set the default to 'string' because if nothing can parse, we will just assume
        // it's a string
        String typeToReturn = "string";

        // The first thing we can do is look for either an empty string or null value
        if (value == null || value.equals("")) {
            logger.debug("Value was empty or null, so returning null type");
            typeToReturn = "null";
        } else {
            // First, let's see if it can be parsed into a Boolean
            Boolean valueAsBoolean = null;
            try {
                if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false")) {
                    valueAsBoolean = Boolean.parseBoolean(value);
                }
            } catch (Exception e) {
                logger.trace("Exception caught trying to convert value to boolean: {}", e.getMessage());
            }

            // Check to see if it's a boolean
            if (valueAsBoolean != null) {
                typeToReturn = "boolean";
            } else {
                // Since it's not a boolean, let's try to convert to a long
                Long valueAsLong = null;
                try {
                    valueAsLong = Long.parseLong(value);
                } catch (NumberFormatException e) {
                    logger.trace("NumberFormatException caught trying to convert value to Long: {}", e.getMessage());
                }

                // Now see if Long parse was successful
                if (valueAsLong != null) {
                    typeToReturn = "long";
                } else {
                    // Still haven't found it yet, let's try to parse to a Double
                    Double valueAsDouble = null;
                    try {
                        valueAsDouble = Double.parseDouble(value);
                    } catch (NumberFormatException e) {
                        logger.trace("NumberFormatException caught trying to convert value to Double: {}",
                                e.getMessage());
                    }

                    // Now check to see if Double was parsed OK
                    if (valueAsDouble != null) {
                        typeToReturn = "double";
                    } else {
                        // Nothing yet, so we are down to almost being a string, but let's check for a
                        // date first. We
                        // will use the JODA DateTime and just use the default parser for now.
                        DateTime valueAsDateTime = null;
                        try {
                            valueAsDateTime = new DateTime(value.trim());
                        } catch (Exception e) {
                            logger.trace("Exception caught trying to convert value to DateTime: {}", e.getMessage());
                        }
                        // Check to see if parsing was successful
                        if (valueAsDateTime != null) {
                            typeToReturn = "date";
                        }
                    }
                }
            }
        }

        // Now return the type that we tried to deduce
        return typeToReturn;
    }

    /**
     * This method takes in a string that might be an XML document. It will try to
     * parse that into a JSONObject and will return a new JSONObject if the
     * conversion was successful
     *
     * @param xmlString
     * @return
     */
    public static JSONObject convertXMLToJSON(String xmlString) {
        logger.debug("convertXMLToJSON called");

        // The JSONObject to return
        JSONObject jsonObjectToReturn = null;

        // Make sure the string is not null
        if (xmlString != null) {
            if (xmlString.length() > 100) {
                logger.debug("XML starts with: {}", xmlString.substring(0, 100));
            } else {
                logger.debug("XML is: {}", xmlString);
            }
            try {
                jsonObjectToReturn = XML.toJSONObject(xmlString);
            } catch (JSONException e) {
                logger.warn("JSONException caught trying to convert XML to JSONObject: {}", e.getMessage());
            }
        }

        // Here is the
        return jsonObjectToReturn;
    }

    /**
     * This method takes in a JSONObject and returns a 'flattened' version of it.
     * This is necessary for indexing raw JSONObject into elasticsearch without
     * explicit mappings.
     *
     * @param jsonObject
     * @return
     */
    public static Map<String, Object> flattenObject(JSONObject jsonObject) {
        // The map to return
        Map<String, Object> mapToReturn = null;

        try {
            mapToReturn = JsonFlattener.flattenAsMap(jsonObject.toString());
        } catch (Exception e) {
            logger.warn("Exception trying to flatten JSON Object: {}", jsonObject.toString());
        }

        // Now return the result
        return mapToReturn;
    }

    /**
     * This method takes in a string key and a value object and returns a JSONObject
     * that is a flattened representation of a JSON property leaf.
     *
     * @param key
     * @param value
     * @return
     */
    public static JSONObject convertKeyValueToFlatObject(String key, Object value) {
        // Create the JSONObject to return
        JSONObject jsonObjectToReturn = null;

        // Make sure we have something coming in
        if (key != null && !key.equals("") && value != null) {
            jsonObjectToReturn = new JSONObject();
            jsonObjectToReturn.put("key", key);

            // Now detect what the value is using it's string representation and add the
            // type and key type
            String valueType = getDataTypeFromString(value.toString());
            jsonObjectToReturn.put("type", valueType);
            jsonObjectToReturn.put("key_type", key + "." + valueType);

            // Now based on the type, assign the value
            if (valueType.equals("string")) {
                jsonObjectToReturn.put("value_string", value.toString());
            } else if (valueType.equals("null")) {
                jsonObjectToReturn.put("value_null", true);
            } else if (valueType.equals("boolean")) {
                // Convert the value
                Boolean valueBoolean = Boolean.parseBoolean(value.toString());
                jsonObjectToReturn.put("value_boolean", valueBoolean);
            } else if (valueType.equals("date")) {
                // Convert the value
                DateTime valueDateTime = new DateTime(value.toString());
                jsonObjectToReturn.put("value_date", valueDateTime);
            } else if (valueType.equals("long")) {
                // Convert the value
                Long valueLong = Long.parseLong(value.toString());
                jsonObjectToReturn.put("value_long", valueLong);
            } else if (valueType.equals("double")) {
                // Convert the value
                Double valueDouble = Double.parseDouble(value.toString());
                jsonObjectToReturn.put("value_double", valueDouble);
            }
        }

        // Now return the result
        return jsonObjectToReturn;
    }

}
