package org.mbari.resources;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;

/**
 * This class contains some utility methods that are used across multiple
 * classes in this package
 */
public class RepositoryIndexerUtils {

    // Create the logger
    private static Logger logger = LogManager.getLogger(RepositoryIndexerUtils.class);

    // Some constants
    private static final String ELASTICSEARCH = "elasticsearch";
    private static final String RABBITMQ = "rabbitmq";
    private static final String ROUTING_KEY = "routingKey";

    /**
     * This method takes in a filename which should be a JSON file and then looks
     * for it on the classpath and converts the file contents to a JSON object and
     * returns it
     *
     * @param filename
     * @return
     */
    public static JSONObject readJSONObjectFromFile(String filename) {
        logger.debug("Reading configuration from file named {}", filename);

        // The JSONObject to return
        JSONObject jsonObjectToReturn = null;

        // Grab this class so we can get to the classpath
        Class clazz = ResourceIndexerUtils.class;

        // Create an input stream trying to get the file from the classpath
        InputStream inputStream = clazz.getResourceAsStream(filename);

        // Since the previous line looks for files on the classpath, if it comes back
        // with nothing we can
        // just check to see if the filename is a path and create the InputStream that
        // way
        if (inputStream == null) {
            try {
                inputStream = new FileInputStream(filename);
            } catch (FileNotFoundException e) {
                logger.error(
                        "Looking for file with name {} in the classpath and did not find it, so"
                                + " looked for it like it was a full path and caught an FileNotFoundException: {}",
                        filename, e.getMessage());
            }
        }

        // If an input stream was created OK, read in all the text into a string builder
        if (inputStream != null) {
            StringBuilder stringBuilder = new StringBuilder();
            try {
                try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStream))) {
                    String line;
                    while ((line = br.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                }
            } catch (IOException e) {
                logger.error("IOException caught trying to read JSON from file {}: {}", filename, e.getMessage());
            }

            // Now, try to convert it to a JSON Object
            logger.debug("JSON read from file");
            String jsonString = stringBuilder.toString();
            try {
                jsonObjectToReturn = new JSONObject(jsonString);
            } catch (JSONException e) {
                logger.error("JSONException caught trying to convert this string to JSONObject: {}", jsonString);
                logger.error(e.getMessage());
            }
        }

        // Return the object
        return jsonObjectToReturn;
    }

    /**
     * This method takes in a JSONObject and loads any pertinent environment
     * variables
     *
     * @param configObject
     */
    public static void loadPropertiesFromEnvironment(JSONObject configObject) {

        // First, let's make sure the object coming in is not null
        if (configObject != null) {
            // Make sure there are elasticsearch and rabbitmq object in the config
            if (!configObject.has(ELASTICSEARCH))
                configObject.put(ELASTICSEARCH, new JSONObject());
            if (!configObject.has(RABBITMQ))
                configObject.put(RABBITMQ, new JSONObject());

            // ***************************************************************************
            // Application properties
            // ***************************************************************************

            // Check to see if the environment states if a FileCrawler is to be started
            if (System.getenv("FTESI_FILE_CRAWLER") != null) {
                configObject.put("fileCrawler", Boolean.parseBoolean(System.getenv("FTESI_FILE_CRAWLER")));
            }

            // Check to see if the environment states if a FSMonEventConsumer is to be
            // started
            if (System.getenv("FTESI_FSMON_EVENT_CONSUMER") != null) {
                configObject.put("fsMonEventConsumer",
                        Boolean.parseBoolean(System.getenv("FTESI_FSMON_EVENT_CONSUMER")));
            }

            // Check to see if the environment states where the data repo is located
            if (System.getenv("FTESI_DATA_REPO") != null) {
                // Then set (or overwrite) the data repository location in the config object
                configObject.put("dataRepo", System.getenv("FTESI_DATA_REPO"));
            }

            // Check to see if the environment states the name of the repo
            if (System.getenv("FTESI_REPO_NAME") != null) {
                configObject.put("repoName", System.getenv("FTESI_REPO_NAME"));
            }

            // Look for network mount flag
            if (System.getenv("FTESI_NETWORK_MOUNT") != null) {
                configObject.put("networkMount", Boolean.parseBoolean(System.getenv("FTESI_NETWORK_MOUNT")));
            }

            // Look for the base URL that matches the repo
            if (System.getenv("FTESI_DATA_REPO_BASE_URL") != null) {
                configObject.put("dataRepoBaseURL", System.getenv("FTESI_DATA_REPO_BASE_URL"));
            }

            // Check for a variable that defines how long to pause between crawls
            if (System.getenv("FTESI_INTERVAL_BETWEEN_REPO_CRAWLS_IN_MINUTES") != null) {
                configObject.put("intervalBetweenRepoCrawlsInMinutes",
                        Integer.parseInt(System.getenv("FTESI_INTERVAL_BETWEEN_REPO_CRAWLS_IN_MINUTES")));
            }

            // ***************************************************************************
            // Elasticsearch properties
            // ***************************************************************************

            // Now let's check to see if the Elasticsearch host is defined
            if (System.getenv("FTESI_ELASTICSEARCH_HOST") != null) {
                // Now write the host
                configObject.getJSONObject(ELASTICSEARCH).put("host", System.getenv("FTESI_ELASTICSEARCH_HOST"));
            }

            // Now let's check to see if the Elasticsearch port number is defined
            if (System.getenv("FTESI_ELASTICSEARCH_PORT") != null) {
                // Now write the port
                configObject.getJSONObject(ELASTICSEARCH).put("port",
                        Integer.parseInt(System.getenv("FTESI_ELASTICSEARCH_PORT")));
            }

            // Now let's check to see if the Elasticsearch username is defined
            if (System.getenv("FTESI_ELASTICSEARCH_USERNAME") != null) {
                // Now write the property
                configObject.getJSONObject(ELASTICSEARCH).put("username",
                        System.getenv("FTESI_ELASTICSEARCH_USERNAME"));
            }

            // Now let's check to see if the Elasticsearch password is defined
            if (System.getenv("FTESI_ELASTICSEARCH_PASSWORD") != null) {
                // Now write the property
                configObject.getJSONObject(ELASTICSEARCH).put("password",
                        System.getenv("FTESI_ELASTICSEARCH_PASSWORD"));
            }

            // ***************************************************************************
            // RabbitMQ properties
            // ***************************************************************************

            // RabbitMQ hostname
            if (System.getenv("FTESI_RABBITMQ_HOST") != null) {
                // Now write the host
                configObject.getJSONObject(RABBITMQ).put("host", System.getenv("FTESI_RABBITMQ_HOST"));
            }

            // RabbitMQ port
            if (System.getenv("FTESI_RABBITMQ_PORT") != null) {
                configObject.getJSONObject(RABBITMQ).put("port",
                        Integer.parseInt(System.getenv("FTESI_RABBITMQ_PORT")));
            }

            // RabbitMQ vhost
            if (System.getenv("FTESI_RABBITMQ_VHOST") != null) {
                configObject.getJSONObject(RABBITMQ).put("vhost", System.getenv("FTESI_RABBITMQ_VHOST"));
            }

            // RabbitMQ username
            if (System.getenv("FTESI_RABBITMQ_USERNAME") != null) {
                configObject.getJSONObject(RABBITMQ).put("username", System.getenv("FTESI_RABBITMQ_USERNAME"));
            }

            // RabbitMQ password
            if (System.getenv("FTESI_RABBITMQ_PASSWORD") != null) {
                configObject.getJSONObject(RABBITMQ).put("password", System.getenv("FTESI_RABBITMQ_PASSWORD"));
            }

            // RabbitMQ exchange name
            if (System.getenv("FTESI_RABBITMQ_EXCHANGE_NAME") != null) {
                configObject.getJSONObject(RABBITMQ).put("exchangeName",
                        System.getenv("FTESI_RABBITMQ_EXCHANGE_NAME"));
            }

            // RabbitMQ queue name
            if (System.getenv("FTESI_RABBITMQ_QUEUE_NAME") != null) {
                configObject.getJSONObject(RABBITMQ).put("queueName", System.getenv("FTESI_RABBITMQ_QUEUE_NAME"));
            }

            // RabbitMQ routing key
            if (System.getenv("FTESI_RABBITMQ_ROUTING_KEY") != null) {
                configObject.getJSONObject(RABBITMQ).put(ROUTING_KEY, System.getenv("FTESI_RABBITMQ_ROUTING_KEY"));
            } else {
                // If there is no routing key, define a default one
                if (!configObject.getJSONObject(RABBITMQ).has(ROUTING_KEY)) {
                    configObject.getJSONObject(RABBITMQ).put(ROUTING_KEY, "*");
                }
            }
        } else {
            logger.warn("Incoming ObjectNode was null");
        }
    }
}
