package org.mbari.resources;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * This class looks for a config.json file on the classpath and loads the
 * configuration from that file. It also grabs a list of
 * directories/repositories that it should be crawling from a file named
 * repos.json. For each directory/repository, it recursively iterates through
 * all the files and hands off the file to a <code>ResourceIndexer</code> (along
 * with an <code>ElasticsearchDAO</code> so that the file can be checked against
 * the information in the Elasticsearch DB and updated if necessary.
 */
public class FileCrawler extends Thread {
    // The logger
    private static Logger logger = LogManager.getLogger(FileCrawler.class);

    // The interface to the Elasticsearch server
    private ElasticsearchDAO elasticsearchDAO = null;

    // The length of time in minutes to wait between starting the next repository
    // crawling
    private Integer intervalBetweenRepoCrawlsInMinutes = 0;

    // This is the object that will take in Resources (and any event information)
    // and index them into Elasticsearch
    private ResourceIndexer resourceIndexer = null;

    // This is the String representation of the path to the data repository to crawl
    private File repoDir = null;

    // A boolean to indicate if the repository to crawl is a network mount
    private Boolean networkMount = null;

    // The array of strings that serve as a list of base locators for files in the
    // repo
    private String[] locatorBaseURIs = null;

    // The name of the repository associated with this file crawler
    private String repoName = null;

    /**
     * This is the constructor for the <code>FileCrawler</code>
     */
    public FileCrawler(ElasticsearchDAO elasticsearchDAO, ResourceIndexer resourceIndexer,
            Integer intervalBetweenRepoCrawlsInMinutes, String dataRepo, Boolean networkMount, String[] locatorBaseURIs,
            String repoName) {

        // Set the elasticsearch interface object and the resource indexer
        this.elasticsearchDAO = elasticsearchDAO;
        this.resourceIndexer = resourceIndexer;

        // Set the interval between crawls
        this.intervalBetweenRepoCrawlsInMinutes = intervalBetweenRepoCrawlsInMinutes;
        logger.info("intervalBetweenRepoCrawlsInMinutes: {}", this.intervalBetweenRepoCrawlsInMinutes);

        // Create the file object that points to the repository to crawl
        this.repoDir = new File(dataRepo);
        logger.info("Going to crawl repo {}", repoDir.getAbsolutePath());

        // Set the repo path and if it's a network mount
        this.networkMount = networkMount;

        // Set the list of base locator URIs
        this.locatorBaseURIs = locatorBaseURIs;

        // The name of the repo
        this.repoName = repoName;
    }

    /**
     * This method starts a loop that reads in the repos.json file and crawls each
     * repo and indexes the files inside the repository. It pauses for
     * intervalBetweenRepoCrawlsInMinutes minutes between loops through all the
     * repos.
     *
     * @return
     */
    @Override
    public void run() {

        logger.info("Starting infinite loop of indexing documents at {}", new Date());

        // Make sure Elasticsearch is ready
        while (!elasticsearchDAO.isServerReady()) {
            logger.warn("Elasticsearch server does not appear to be ready, will sleep for 10 seconds and try again");
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                logger.error("InterruptedException caught trying to sleep FileCrawler run for 10 seconds");
                Thread.currentThread().interrupt();
            }
        }

        // Start an infinite loop
        do {
            logger.debug("Starting indexing ...");

            // Make sure it's a directory and it exists
            if (repoDir != null && repoDir.exists() && repoDir.isDirectory()) {

                // Now walk the directory and index each entry
                try {
                    // TODO kgomes - I need to filter on file patterns to ignore here somewhere
                    // TODO kgomes - This entire thing fails and stops walking the tree if one of
                    // the files throws exception, need to fix this!
                    Files.walk(Paths.get(repoDir.getAbsolutePath()))
                            .forEach(fileToIndex -> resourceIndexer.indexFile(fileToIndex, this.repoName,
                                    locatorBaseURIs, repoDir.getAbsolutePath(), networkMount));
                } catch (IOException e) {
                    logger.error("IOException caught trying to walk path {}: {}", repoDir.getAbsolutePath(),
                            e.getMessage());
                } catch (Exception e) {
                    logger.error("Exception caught trying to index files: {}", e.getMessage());
                }
            } else {
                if (repoDir == null) {
                    logger.warn("The local directory linked to repository was null so it will be skipped");
                } else {
                    logger.warn(
                            "The local directory ({}) linked to repository {} either does not exist or "
                                    + " is not a directory, so it will be skipped",
                            repoDir.getAbsolutePath(), this.repoName);
                }
            }

            // So we have walked the repository and indexed anything found into
            // Elasticsearch, we should now go the other direction and crawl the
            // elasticsearch repo and if any of the files listed there, are not found in the
            // local directory, they should have their elasticsearch document removed. Since
            // this can be a large process with many results to iterate over, we will do it
            // in blocks. First get the total number of documents in the repository
            long documentCount = elasticsearchDAO.getDocumentCount(this.repoName);
            if (documentCount > 0) {
                logger.info("Will check {} documents in repo {} to see if they exist locally "
                        + "and will remove them if the files do not exist", documentCount, this.repoName);

                // The number of documents checked
                int numDocsChecked = 0;

                // Break it up into batches of 10, yes, down casting is dangerous.
                int numberOfBatches = (int) documentCount / 10;
                int modNumberOfBatches = (int) documentCount % 10;
                if (modNumberOfBatches > 0)
                    numberOfBatches = numberOfBatches + 1;

                // Now iterate over the number of batches
                for (int bn = 0; bn < numberOfBatches; bn++) {

                    JSONArray uris = elasticsearchDAO.getAllURIsFromIndex(this.repoName, bn * 10, 10);

                    // Now iterate over the uris
                    for (Object uriObject : uris) {

                        numDocsChecked++;

                        // Grab the UUID and URI and see if it exists locally
                        String uuid = ((JSONObject) uriObject).getString("uuid");

                        URI uri = null;
                        try {
                            uri = new URI(((JSONObject) uriObject).getString("uri"));
                        } catch (URISyntaxException e) {
                            logger.error("Could not convert file URI {} to URI object: {}",
                                    ((JSONObject) uriObject).getString("uri"), e.getMessage());
                        }

                        if (uri != null) {
                            File localFileToCheck = new File(uri);
                            if (localFileToCheck.exists()) {
                                logger.debug("File with URI {} does exist, will not remove", uri);
                            } else {
                                logger.debug("File with URI {} does NOT exist, document with UUID {} "
                                        + "will be removed from elasticsearch", uri, uuid);
                                elasticsearchDAO.removeResourceByUUID(this.repoName, uuid);
                            }
                        }
                    }
                }
                logger.debug("There were {} documents to check and {} were checked", documentCount, numDocsChecked);
                if (numDocsChecked != documentCount) {
                    logger.warn(
                            "In the clean out phase, there were supposedly {} documents to check, "
                                    + "but {} were checked.  Could be a timing thing, but something to watch",
                            documentCount, numDocsChecked);
                }
            }

            // Sleep for the allotted number of minutes
            try {
                logger.debug("Done looping through repositories, will sleep for {} minutes.",
                        intervalBetweenRepoCrawlsInMinutes);
                Thread.sleep(intervalBetweenRepoCrawlsInMinutes * 60000);
            } catch (InterruptedException e) {
                logger.error("Exception trying to sleep for {} minutes.", intervalBetweenRepoCrawlsInMinutes);
                Thread.currentThread().interrupt();
            }
        } while (true);
    }
}
