#!/bin/sh

# Create Rabbitmq user
( rabbitmqctl wait --timeout 60 $RABBITMQ_PID_FILE ; \
rabbitmqctl add_user $RABBITMQ_DEFAULT_USER $RABBITMQ_DEFAULT_PASS 2>/dev/null ; \
rabbitmqctl set_user_tags $RABBITMQ_DEFAULT_USER administrator management ; \
rabbitmqctl set_permissions -p / $RABBITMQ_DEFAULT_USER  ".*" ".*" ".*" ; \
rabbitmqctl add_vhost $RABBITMQ_DEFAULT_VHOST ; \
rabbitmqctl add_user $RABBITMQ_CLIENT_USER $RABBITMQ_CLIENT_PASS 2>/dev/null ; \
rabbitmqctl set_permissions -p $RABBITMQ_DEFAULT_VHOST $RABBITMQ_DEFAULT_USER  ".*" ".*" ".*" ; \
rabbitmqctl set_permissions -p $RABBITMQ_DEFAULT_VHOST $RABBITMQ_CLIENT_USER  ".*" ".*" ".*" ; \
echo "*** User '$RABBITMQ_CLIENT_USER' completed. ***") & \

# $@ is used to pass arguments to the rabbitmq-server command.
# For example if you use it like this: docker run -d rabbitmq arg1 arg2,
# it will be as you run in the container rabbitmq-server arg1 arg2
rabbitmq-server $@
