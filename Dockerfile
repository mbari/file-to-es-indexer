# Start with the OpenJDK image
FROM openjdk:8

# Add the jar file
COPY ./target/file-indexer-0.0.11-jar-with-dependencies.jar /usr/src/ftesi/ftesi.jar

# Set the working directory
WORKDIR /usr/src/ftesi

# Run the application
CMD java -cp /usr/src/ftesi:/usr/src/ftesi/ftesi.jar org.mbari.resources.RepositoryIndexer
