#!/bin/bash
# This file sets up the directory structure that is needed to run the docker-compose.yml file and then
# starts up all the services with docker compose

# First, read the environment variables from the .env file
source .env
echo "Starting setup of the file-to-es-indexer repository structure in directory ${FTESI_BASEDIR_HOST}"

###########################################
# Create the logging directory
###########################################
if [ ! -d ${FTESI_BASEDIR_HOST}/logs ]; then
    echo "Creating ${FTESI_BASEDIR_HOST}/logs"
    mkdir ${FTESI_BASEDIR_HOST}/logs
else
    echo "${FTESI_BASEDIR_HOST}/logs already exists"
fi

###########################################
# RabbitMQ server directories and files
###########################################
# Now create the directory structure and install the configuration files for the RabbitMQ server.
if [ ! -d ${FTESI_BASEDIR_HOST}/rabbitmq ]; then
    echo "Creating ${FTESI_BASEDIR_HOST}/rabbitmq"
    mkdir ${FTESI_BASEDIR_HOST}/rabbitmq
else
    echo "${FTESI_BASEDIR_HOST}/rabbitmq already exists"
fi
if [ ! -d ${FTESI_BASEDIR_HOST}/rabbitmq/mnesia ]; then
    echo "Creating ${FTESI_BASEDIR_HOST}/rabbitmq/mnesia"
    mkdir ${FTESI_BASEDIR_HOST}/rabbitmq/mnesia
else
    echo "${FTESI_BASEDIR_HOST}/rabbitmq/mnesia already exists"
fi
if [ ! -d ${FTESI_BASEDIR_HOST}/rabbitmq/logs ]; then
    echo "Creating ${FTESI_BASEDIR_HOST}/rabbitmq/logs"
    mkdir ${FTESI_BASEDIR_HOST}/rabbitmq/logs
else
    echo "${FTESI_BASEDIR_HOST}/rabbitmq/logs already exists"
fi

###########################################
# The Elasticsearch server
###########################################
if [ ! -d ${FTESI_BASEDIR_HOST}/elasticsearch ]; then
    echo "Creating ${FTESI_BASEDIR_HOST}/elasticsearch"
    mkdir ${FTESI_BASEDIR_HOST}/elasticsearch
else
    echo "${FTESI_BASEDIR_HOST}/elasticsearch already exists"
fi
if [ ! -d ${FTESI_BASEDIR_HOST}/elasticsearch/data ]; then
    echo "Creating ${FTESI_BASEDIR_HOST}/elasticsearch/data"
    mkdir ${FTESI_BASEDIR_HOST}/elasticsearch/data
else
    echo "${FTESI_BASEDIR_HOST}/elasticsearch/data already exists"
fi
if [ ! -d ${FTESI_BASEDIR_HOST}/elasticsearch/logs ]; then
    echo "Creating ${FTESI_BASEDIR_HOST}/elasticsearch/logs"
    mkdir ${FTESI_BASEDIR_HOST}/elasticsearch/logs
else
    echo "${FTESI_BASEDIR_HOST}/elasticsearch/logs already exists"
fi

###########################################
# The Kibana Server
###########################################
if [ ! -d ${FTESI_BASEDIR_HOST}/kibana ]; then
    echo "Creating ${FTESI_BASEDIR_HOST}/kibana"
    mkdir ${FTESI_BASEDIR_HOST}/kibana
else
    echo "${FTESI_BASEDIR_HOST}/kibana already exists"
fi
if [ ! -d ${FTESI_BASEDIR_HOST}/kibana/data ]; then
    echo "Creating ${FTESI_BASEDIR_HOST}/kibana/data"
    mkdir ${FTESI_BASEDIR_HOST}/kibana/data
else
    echo "${FTESI_BASEDIR_HOST}/kibana/data already exists"
fi
if [ ! -d ${FTESI_BASEDIR_HOST}/kibana/logs ]; then
    echo "Creating ${FTESI_BASEDIR_HOST}/kibana/logs"
    mkdir ${FTESI_BASEDIR_HOST}/kibana/logs
else
    echo "${FTESI_BASEDIR_HOST}/kibana/logs already exists"
fi

###########################################
# Run Docker Compose up
###########################################
docker-compose up
